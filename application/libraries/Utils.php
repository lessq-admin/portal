<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Utils {

	public function __construct()
	{
	    $this->CI =& get_instance();
	}

    function get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date)
    {
		$dateObj = DateTime::createFromFormat('Y-m-d', $date);
		$dayofweek = $dateObj->format('w');

		$data_array = $this->CI->shift->get_regular_shift_for_hospital_doctor_day_of_week($hospital_id, $doctor_id, $dayofweek);
		//check for cancelations
		foreach ($data_array as $row)
        {
			$row->type = 'Regular';
		}
		//get special shifts
		$special_shifts = $this->CI->shift->get_special_shift_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
		foreach ($special_shifts as $row)
        {
        	$row->type = 'Special';
      		$data_array[] = $row;
		}
		return $data_array;
	}

	function get_dailyq_for_hospital_doctor($hospital_id, $doctor_id) 
	{
		$data_array = $this->CI->dailyq->get_queues_for_hospital_doctor($hospital_id, $doctor_id);
		foreach ($data_array as $row)
        {
        	if($row->shift_regular_id != NULL)
        		$row->type = 'Regular';
        	else if($row->shift_special_id != NULL)
        		$row->type = 'Special';
        	else
        		$row->type = '--'; //for future usage
		}
		return $data_array;
	}
}

/* End of file Someclass.php */