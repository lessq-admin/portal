<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        return $var;
    }   
}


if ( ! function_exists('get_current_date_time'))
{
    function get_current_date_time()
    {
        $date = new DateTime();
      	$val = $date->format('Y-m-d H:i:s');
      	return $val;
    }   
}

if ( ! function_exists('get_current_date'))
{
    function get_current_date()
    {
        $date = new DateTime();
        $val = $date->format('Y-m-d');
        return $val;
    }   
}

if ( ! function_exists('get_current_time'))
{
    function get_current_time()
    {
        $date = new DateTime();
        $val = $date->format('H:i');
        return $val;
    }   
}

if ( ! function_exists('validate_date'))
{
    function validate_date($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

function asset_url(){
   return base_url().'assets/';
}

// private function crypto_rand_secure($min, $max) {
//         $range = $max - $min;
//         if ($range < 0) return $min; // not so random...
//         $log = log($range, 2);
//         $bytes = (int) ($log / 8) + 1; // length in bytes
//         $bits = (int) $log + 1; // length in bits
//         $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
//         do {
//             $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
//             $rnd = $rnd & $filter; // discard irrelevant bits
//         } while ($rnd >= $range);
//         return $min + $rnd;

// 	}

// 	private function getToken($length){
// 	    $token = "";
// 	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// 	    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
// 	    $codeAlphabet.= "0123456789";
// 	    for($i=0;$i<$length;$i++){
// 	        $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	        
// 	    }
// 	    return $token;
// 	}