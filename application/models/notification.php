<?php

class Notification extends CI_Model
{

	public function get_qdata_for_notification($dailyq_id)
	{
		$this -> db -> select("hospital.name as hospital_name, doctor.display_name as doctor_display_name, dailyq.patient_count, DATE_FORMAT(dailyq.updated, '%H:%i') as updated, DATE_FORMAT(dailyq.started, '%H:%i') as started, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
		$this -> db -> from(' dailyq, sms_code, hospital, doctor');
		$this -> db -> where('dailyq.sms_code_id  = sms_code.id');
		$this -> db -> where('sms_code.doctor_id  = doctor.id');
		$this -> db -> where('sms_code.hospital_id = hospital.id');
		$this -> db -> where('dailyq.id', $dailyq_id);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_today_notification_for_queue_id($dailyq_id)
	{
		$this->db-> from('notification');
		$this->db->where('dailyq_id', $dailyq_id);
		$this->db->where('status', 1);
		$this->db->where('time_stamp >= CURDATE()');
		$this->db->where('time_stamp < CURDATE() + INTERVAL 1 DAY');

		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

   public function add_notification($dailyq_id, $notification, $status)
   {
      $data = array(
         'time_stamp' => get_current_date_time(),
         'dailyq_id' => $dailyq_id,
         'message' => $notification,
         'status' => $status
         );
      $this->db->trans_start();
      $this->db->insert('notification',$data);
      $this->db->trans_complete();
   }

   public function update_notification_status($notification_id, $status)
   {
      	$data = array(
        	'status' => $status
        );
        $this->db->where('id', $notification_id);
        $this->db->update('notification', $data);
   }

}

?>