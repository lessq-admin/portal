<?php

class Dailyq_log extends CI_Model
{
    /*
    Status field of the dailyq
    1: start q : 1
    2: update q : 2
    3: finish q : 3
    4: reset q : 4
    5: standby q:5
    100: first digit is the status and last two 00 are error



    */


    // function insert($shift_id, $status, $patient_cnt)
    // {
    //     $data = array(
    //       'shift_id' => $shift_id,
    //       'time_stamp' => get_current_date_time(),
    //       'msg_status' => $status,
    //       'patient_count' => $patient_cnt
    //       );
    //     $this->db->trans_start();
    //     $this->db->insert('dailyq_log',$data);
    //     $this->db->trans_complete();
    // }

    //todo:done
    function insert($status, $patient_cnt, $shift_type, $shift_special_id, $shift_regular_id, $daily_q_status)
    {
        $data = array(
          'time_stamp' => get_current_date_time(),
          'msg_status' => $status,
          'patient_count' => $patient_cnt,
          'shift_type' => $shift_type,
          'shift_special_id' => $shift_special_id,
          'shift_regular_id' => $shift_regular_id,
          'q_status' => $daily_q_status
          );
        $this->db->trans_start();
        $this->db->insert('dailyq_log',$data);
        $this->db->trans_complete();
    }

    function get_shift_regular_logs($shift_regular_id, $date) 
    {
      $next_date = date('Y-m-d 03:59:00', strtotime($date .' +1 day'));
      $from_date = date('Y-m-d 04:30:00', strtotime($date));

      $this -> db -> select("id, DATE_FORMAT(time_stamp, '%H:%i') as time_stamp, patient_count, msg_status", FALSE);
      $this -> db -> from('dailyq_log');
      $this -> db -> where('shift_regular_id', $shift_regular_id);
      $this -> db -> where('time_stamp >=', $from_date);
      $this -> db -> where('time_stamp <=', $next_date); 
      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      }
    }

    function get_shift_special_logs($shift_special_id, $date) 
    {
      $next_date = date('Y-m-d 03:59:00', strtotime($date .' +1 day'));
      $from_date = date('Y-m-d 04:30:00', strtotime($date));

      $this -> db -> select("id, DATE_FORMAT(time_stamp, '%H:%i') as time_stamp, patient_count, msg_status", FALSE);
      $this -> db -> from('dailyq_log');
      $this -> db -> where('shift_special_id', $shift_special_id);
      $this -> db -> where('time_stamp >=', $from_date);
      $this -> db -> where('time_stamp <=', $next_date); 
      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      }
    }

    // function get_distinct_shift_ids_for($hospital_id, $date)
    // {
    //   $next_date = date('Y-m-d', strtotime($date .' +1 day'));

    //   $this -> db -> select("distinct shift_regular_id, distinct shift_special_id", FALSE);
    //   $this -> db -> from('dailyq_log');
    //   $this -> db -> where('shift.hospital_id', $hospital_id);
    //   $this -> db -> where('time_stamp >=', $date);
    //   $this -> db -> where('time_stamp <', $next_date); 
    //   $query = $this -> db -> get();

    //   if($query -> num_rows() > 0)
    //   {
    //    return $query->result();
    //   }
    //   else
    //   {
    //    return false;
    //   } 
    // }
//todo:done
    function get_regular_shift_ids_for_hospital_date($hospital_id, $date)
    {
      $next_date = date('Y-m-d 03:59:00', strtotime($date .' +1 day'));
      $from_date = date('Y-m-d 04:30:00', strtotime($date));

      $this -> db -> select("distinct shift_regular_id", FALSE);
      $this -> db -> from('dailyq_log, shift_regular, sms_code, hospital');
      $this -> db -> where('dailyq_log.shift_type', 1);
      $this -> db -> where('dailyq_log.time_stamp >=', $from_date);
      $this -> db -> where('dailyq_log.time_stamp <', $next_date); 
      $this -> db -> where('dailyq_log.shift_regular_id = shift_regular.id');
      $this -> db -> where('shift_regular.sms_code_id = sms_code.id');
      $this -> db -> where('sms_code.hospital_id = hospital.id');
      $this -> db -> where('hospital.id', $hospital_id);

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      } 
    }

    function get_special_shift_ids_for_hospital_date($hospital_id, $date)
    {
      $next_date = date('Y-m-d 03:59:00', strtotime($date .' +1 day'));
      $from_date = date('Y-m-d 04:30:00', strtotime($date));

      $this -> db -> select("distinct shift_special_id", FALSE);
      $this -> db -> from('dailyq_log, shift_special, sms_code, hospital');
      $this -> db -> where('dailyq_log.shift_type', 2);
      $this -> db -> where('dailyq_log.time_stamp >=', $from_date);
      $this -> db -> where('dailyq_log.time_stamp <', $next_date); 
      $this -> db -> where('dailyq_log.shift_special_id = shift_special.id');
      $this -> db -> where('shift_special.sms_code_id = sms_code.id');
      $this -> db -> where('sms_code.hospital_id = hospital.id');
      $this -> db -> where('hospital.id', $hospital_id);

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      } 
    }


}
?>