<?php
Class Shift extends CI_Model
{
   // function get_shift_for_hospital($hospital_id)
   // {
   //    $date = new DateTime();
   //    $dayofweek = $date->format('w');
   //    $this -> db -> select("doctor.id as doctor_id, doctor.display_name as doctor_display_name, shift.id as shift_id, DATE_FORMAT(shift.start_time, '%H:%i') as start_time, DATE_FORMAT(shift.end_time, '%H:%i') as end_time", FALSE);
   //    $this -> db -> from('hospital hospital, doctor doctor, shift shift');
   //    $this -> db -> where('hospital.id = shift.hospital_id');
   //    $this -> db -> where('doctor.id = shift.doctor_id');
   //    //$this -> db -> where("shift.day = date_format(now(), '%w')");
   //    $this -> db -> where('shift.day', $dayofweek);
   //    $this -> db -> where('hospital.id', $hospital_id);

   //    $query = $this -> db -> get();

   //    if($query -> num_rows() > 0)
   //    {
   //       return $query->result_array();
   //    }
   //    else
   //    {
   //       return false;
   //    }
   // }

   // function get_doctor_for_sms_code($sms_code)
   // {
   //    $this -> db -> select("distinct doctor.display_name as doctor_name, hospital.name as hospital_name", FALSE);
   //    $this -> db -> from('shift shift, doctor doctor, hospital hospital');
   //    $this -> db -> where('hospital.id = shift.hospital_id');
   //    $this -> db -> where('doctor.id = shift.doctor_id');
   //    $this -> db -> where('sms_code', $sms_code);

   //    $query = $this -> db -> get();

   //    if($query -> num_rows() > 0)
   //    {
   //       return $query->row();
   //    }
   //    else
   //    {
   //       return false;
   //    }
      
   // }

   // function get_all_shifts_for_hospital($hospital_id) 
   // {
   //    $this -> db -> select("doctor.id as doctor_id, doctor.title, doctor.fname, doctor.mname, 
   //    doctor.lname, doctor.display_name, doctor.speciality, 
   //    shift.id as shift_id, shift.day, 
   //    DATE_FORMAT(shift.start_time, '%H:%i') as start_time, 
   //    DATE_FORMAT(shift.end_time, '%H:%i') as end_time, shift.sms_code", FALSE);
   //    $this -> db -> from('hospital hospital, doctor doctor, shift shift');
   //    $this -> db -> where('hospital.id = shift.hospital_id');
   //    $this -> db -> where('doctor.id = shift.doctor_id');
   //    $this -> db -> where('hospital.id', $hospital_id);
   //    $this -> db -> order_by("display_name", "asc");

   //    $query = $this -> db -> get();

   //    if($query -> num_rows() > 0)
   //    {
   //     return $query->result();
   //    }
   //    else
   //    {
   //     return false;
   //    }
   // }
//todo:done
   function get_regular_shift($regular_shift_id)
   {
      $this -> db -> select("doctor.id as doctor_id, doctor.title, doctor.fname, doctor.mname, 
      doctor.lname, doctor.display_name, doctor.speciality, 
      shift_regular.id as shift_id, shift_regular.status as status, shift_regular.day as day,
      DATE_FORMAT(shift_regular.start_time, '%H:%i') as start_time, 
      DATE_FORMAT(shift_regular.end_time, '%H:%i') as end_time, 
      shift_regular.booking_enabled,
      shift_regular.booking_start_time,
      shift_regular.booking_end_time,
      shift_regular.booking_config_id,
      sms_code.code as sms_code", FALSE);
      $this -> db -> from('hospital, doctor, shift_regular, sms_code');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('sms_code.id = shift_regular.sms_code_id');
      $this -> db -> where('shift_regular.id', $regular_shift_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
       return $query->row();
      }
      else
      {
       return false;
      }
   }
//todo:done
   function get_special_shift($shift_special_id)
   {
      $this -> db -> select("doctor.id as doctor_id, doctor.title, doctor.fname, doctor.mname, 
      doctor.lname, doctor.display_name, doctor.speciality, 
      shift_special.id as shift_id, shift_special.status as status, 
      DATE_FORMAT(shift_special.start_time, '%H:%i') as start_time, 
      DATE_FORMAT(shift_special.end_time, '%H:%i') as end_time, 
      shift_special.booking_enabled,
      shift_special.booking_start_time,
      shift_special.booking_end_time,
      shift_special.booking_config_id,
      sms_code.code as sms_code", FALSE);
      $this -> db -> from('hospital, doctor, shift_special, sms_code');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('sms_code.id = shift_special.sms_code_id');
      $this -> db -> where('shift_special.id', $shift_special_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
       return $query->row();
      }
      else
      {
       return false;
      }
   }
//todo:done
   //Returns regular shifts
   function get_regular_shift_for_hospital_doctor_day_of_week($hospital_id, $doctor_id, $dayofweek)
   {
      $this -> db -> select("doctor.id as doctor_id, doctor.title, doctor.fname, doctor.mname, 
      doctor.lname, doctor.display_name, doctor.speciality, 
      shift_regular.id as shift_id, shift_regular.status as status, shift_regular.day as day,
      DATE_FORMAT(shift_regular.start_time, '%H:%i') as start_time, 
      DATE_FORMAT(shift_regular.end_time, '%H:%i') as end_time, shift_regular.booking_enabled as booking_enabled, sms_code.code as sms_code", FALSE);
      $this -> db -> from('hospital, doctor, shift_regular, sms_code');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('sms_code.id = shift_regular.sms_code_id');
      if($dayofweek != -1)
         $this -> db -> where('shift_regular.day', $dayofweek);
      $this -> db -> where('shift_regular.status', 1);
      $this -> db -> where('hospital.id', $hospital_id);
      if($doctor_id != 0)
         $this -> db -> where('doctor.id', $doctor_id);
      $this -> db -> order_by("display_name", "asc");
      $this -> db -> order_by("day", "asc");
      $this -> db -> order_by("start_time", "asc");

      $query = $this -> db -> get();

      return $query->result();

      // if($query -> num_rows() > 0)
      // {
      //    return $query->result();
      // }
      // else
      // {
      //    return false;
      // }
   }

   //return special shifts
   function get_special_shift_for_hospital_doctor_date($hospital_id, $doctor_id, $date)
   {
      $this -> db -> select("doctor.id as doctor_id, doctor.title, doctor.fname, doctor.mname, 
      doctor.lname, doctor.display_name, doctor.speciality, 
      shift_special.id as shift_id, shift_special.status as status,
      DATE_FORMAT(shift_special.start_time, '%H:%i') as start_time, 
      DATE_FORMAT(shift_special.end_time, '%H:%i') as end_time, shift_special.booking_enabled as booking_enabled, sms_code.code as sms_code", FALSE);
      $this -> db -> from('hospital, doctor, shift_special, sms_code');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('sms_code.id = shift_special.sms_code_id');
      $this -> db -> where('shift_special.date', $date);
      $this -> db -> where('hospital.id', $hospital_id);
      if($doctor_id != 0)
         $this -> db -> where('doctor.id', $doctor_id);
      $this -> db -> order_by("display_name", "asc");

      $query = $this -> db -> get();

      return $query->result();

      // if($query -> num_rows() > 0)
      // {
      //    return $query->result();
      // }
      // else
      // {
      //    return false;
      // }
   }

//todo:done

   function get_cancel_shifts ($shift_regular_id, $date) 
   {
      $this -> db -> select("shift_cancel.id as id, shift_cancel.status as status");
      $this -> db -> from("shift_regular, shift_cancel");
      $this -> db -> where("shift_regular.id = shift_cancel.shift_regular_id");
      $this -> db -> where("shift_cancel.status != 0");
      $this -> db -> where("shift_cancel.date", $date);
      $this -> db -> where("shift_regular.id", $shift_regular_id);
      $cancel_shift_query = $this -> db -> get();
      // if cancelled update the statu of shift_cancel
      if($cancel_shift_query  -> num_rows() == 1)
      {
         return $cancel_shift_query->row();
      }
      else
      {
         return false;
      }
   }
//todo:done


   // function cancel_special_shift($shift_special_id)
   // {
   //    $data = array(
   //       'status' => 0,
   //       'updated' => get_current_date_time()
   //     );
   //     $this->db->where('id', $shift_special_id);
   //     $this->db->update('shift_special', $data); 
   // }

   // //this method is called after daily q operation
   // function inactivate_cancel_shift($cancel_shift_id) 
   // {
   //    $data = array(
   //       'status' => 0,
   //       'updated' => get_current_date_time()
   //     );
   //     $this->db->where('id', $cancel_shift_id);
   //     $this->db->update('shift_cancel', $data); 
   // }

   //this method is used to change the status of the canceled 
   //status->0, 1, 2
   function shift_cancel_set_status($cancel_shift_id, $status) 
   {
      $data = array(
         'status' => $status,
         'updated' => get_current_date_time()
       );
       $this->db->where('id', $cancel_shift_id);
       $this->db->update('shift_cancel', $data); 
   }

   function shift_special_set_status($special_shift_id, $status) 
   {
      $data = array(
         'status' => $status,
         'updated' => get_current_date_time()
       );
       $this->db->where('id', $special_shift_id);
       $this->db->update('shift_special', $data); 
   }


//todo:done
   function cancel_special_shift($shift_special_id)
   {
      $data = array(
         'status' => 0,
         'updated' => get_current_date_time()
       );
       $this->db->where('id', $shift_special_id);
       $this->db->update('shift_special', $data); 
   }

//cancel regular shift by adding an entry to the cancel shift
   function cancel_regular_shift($shift_regular_id, $date)
   {
      $data = array(
         'shift_regular_id' => $shift_regular_id,
         'date' => $date,
         'updated' => get_current_date_time(),
         'status' => 1
       );
       $this->db->insert('shift_cancel', $data); 
       $insert_id = $this->db->insert_id();
       return  $insert_id;
   }

   function add_special_shift($date, $start_time, $end_time, $sms_code_id, $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id)
   {
      $data = array(
         'date' => $date,
         'status' => 1,
         'start_time' => $start_time,
         'end_time' => $end_time,
         'sms_code_id' => $sms_code_id,
         'booking_enabled' => $booking_enabled,
         'booking_start_time' => $booking_start_time,
         'booking_end_time' => $booking_end_time,
         'booking_config_id' => $booking_config_id,
         'updated' => get_current_date_time(),
       );
      $this->db->trans_start();
      $this->db->insert('shift_special',$data);
      $insert_id = $this->db->insert_id();
      $this->db->trans_complete();
      return  $insert_id;
   }

}
?>