<?php
Class Constants extends CI_Model
{

function get_constant($name)
 {
   $this -> db -> from('constants');
   $this -> db -> where('name', $name);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->row();
   }
   else
   {
     return false;
   }
 }
}
?>