<?php
Class Pdu extends CI_Model
{
	//TODO: remove this 
	function get_hospital_id_for_mac($mac_int)
	{
	   $this -> db -> select('hospital_id');
	   $this -> db -> from('pdu');
	   $this -> db -> where('mac_int', $mac_int);
	   $this -> db -> where('status', 1);
	   $this -> db -> limit(1);
	   $query = $this -> db -> get();
	   if($query -> num_rows() == 1)
	   {
	   		return $query->row()->hospital_id;
	   }
	   else
	   {
	     	return false;
	   }
	}

	function get_pdu_for_hospital_room($hospital_id, $room_number) 
	{
		$this -> db -> from('pdu');
	   	$this -> db -> where('hospital_id', $hospital_id);
	   	$this -> db -> where('room_number', $room_number);
	   	$this -> db -> where('status', 1);
	   	$this -> db -> limit(1);
	   	$query = $this -> db -> get();
	   	if($query -> num_rows() == 1)
	   	{
	   		return $query->row();
	   	}
	   	else
	   	{
	     	return false;
	   	}
	}

	//Get PDU
	function get_pdu_for_uid($uid) 
	{
	   	$this -> db -> from('pdu');
	   	$this -> db -> where('uid', $uid);
		$this -> db -> where('status', 1);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
		 	return false;
		}
	}

	function get_pdu_for_id($id) 
	{
	   	$this -> db -> from('pdu');
	   	$this -> db -> where('id', $id);
		$this -> db -> where('status', 1);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->row();
		}
		else
		{
		 	return false;
		}
	}

	function get_pdus() 
	{
		$this -> db -> select('hospital.name, pdu.uid, pdu.description');
		$this -> db -> from('pdu, hospital');
		$this -> db -> where('hospital.id = pdu.hospital_id');
		$query = $this -> db -> get();
		return $query->result();
	}
}
?>