<?php

// 0 for visited booking
// 1 for sms booking

class Dailyb_log extends CI_Model
{
	function insert($shift_type, $shift_special_id, $shift_regular_id, $appointment, $type)
    {
    	$this->db->trans_start();
    	//get the last reference
    	$query = $this->db->select('id, reference')->order_by('id','desc')->limit(1)->get('dailyb_log');
		if($query -> num_rows() == 1)
		{
			$new_ref = $query->row()->reference + 1;
			if($new_ref == 100000)
				$new_ref = 1;
		}
		else
		{
			$new_ref = 1;
		}

        $data = array(
          'time_stamp' => get_current_date_time(),
          'appointment' => $appointment,
          'reference' => $new_ref,
          'type' => $type,
          'shift_type' => $shift_type,
          'shift_special_id' => $shift_special_id,
          'shift_regular_id' => $shift_regular_id
          );
        
        $this->db->insert('dailyb_log',$data);
        $this->db->trans_complete();
        return $new_ref;
    }
}

?>