<?php

/***
status 
0: wrong-lessq
1: lessq
2: wrong-getq
3: getq

**/

class Sms_log extends CI_Model
{   
    function insert_request($message, $carrier)
    {
        $data = array(
          'carrier' => $carrier,
          'request' => $message,
          'request_time' => get_current_date_time()
          );
        $this->db->trans_start();
        $this->db->insert('sms_log',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    function insert_response($id, $message)
    {
        $data = array(
          'response' => $message,
          'response_time' => get_current_date_time()
          );
        $this->db->where('id', $id);
        $this->db->update('sms_log', $data); 

    }

    function insert_result($id, $message)
    {
        $data = array(
          'result' => $message,
          'result_time' => get_current_date_time()
          );
        $this->db->where('id', $id);
        $this->db->update('sms_log', $data); 

    }

    function update_status($id, $status)
    {
        $data = array(
          'status' => $status
          );
        $this->db->where('id', $id);
        $this->db->update('sms_log', $data); 

    }

    function get_sms_log()
    {
      $this -> db -> select("date(request_time) as curr_date, carrier, count(id) as cnt");
      $this -> db -> from("sms_log");
      $this -> db -> group_by(array("curr_date", "carrier")); 

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      }
    }

    function get_sms_log_for_id($sms_log_id)
    {
      // $this -> db -> select()
      $this -> db -> from("sms_log");
      $this -> db -> where('id', $sms_log_id); 

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
       return $query->row();
      }
      else
      {
       return false;
      }
    }

    function get_min_date()
    {
      $this -> db -> select("min(date(request_time)) as min_date");
      $this -> db -> from("sms_log");

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->row()->min_date;
      }
      else
      {
       return false;
      }
    }

    function get_sms_for_dates($from_date, $to_date)
    {
      $next_date = date('Y-m-d', strtotime($to_date .' +1 day'));
      $this -> db -> select("id, carrier, request, response, request_time");
      $this -> db -> from("sms_log");
      $this -> db -> where('request_time >=', $from_date);
      $this -> db -> where('request_time <', $next_date); 

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      }
    }

    function get_sms_for_query($sms_code, $from_date, $to_date, $sms_status)
    {
      //todo change this
      $next_date = date('Y-m-d', strtotime($to_date .' +1 day'));

      $this -> db -> select("sms_log.id, sms_log.carrier, sms_log.request, 
                             sms_log.response, sms_log.result, sms_log.request_time, sms_log.status");
      $this -> db -> from("sms_log");
      if(!empty($sms_code))
        $this -> db -> where("sms_log.request LIKE '%$sms_code%'");

      $this -> db -> where('request_time >=', $from_date);
      $this -> db -> where('request_time <', $next_date); 
      if($sms_status != -1)
        $this -> db -> where('status', $sms_status); 

      $query = $this -> db -> get();

      return $query->result_array();

      // if($query -> num_rows() > 0)
      // {
      //  return $query->result_array();
      // }
      // else
      // {
      //  return false;
      // }
    }
}
?>