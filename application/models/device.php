<?php
Class Device extends CI_Model
{

   function check_uid($uid)
   {
      $this -> db -> select('id');
      $this -> db -> from('device');
      $this -> db -> where('uid', $uid);
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
        return $query->row();
      }
      else
      {
        return false;
      }
    }

    function verify($uid, $passcode)
    {
      //$this -> db -> select('id');
      $this -> db -> from('device');
      $this -> db -> where('uid', $uid);
      $this -> db -> where('passcode', $passcode);
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }

   function get_devices()
   {
      $this -> db -> from('device');
      $query = $this -> db -> get();
      return $query->result();
      
   }

   function get_operational_device()
   {
      $this -> db -> from('device');
      $this -> db -> where('id', '111111');
      $query = $this -> db -> get();
      return $query->row();
   }

   function update_operational_device($hospital_id, $status)
   {
      $data = array(
         'hospital_id' => $hospital_id,
         'status' => $status,
         'updated' => get_current_date_time()
       );
       $this->db->where('id', '111111');
       $this->db->update('device', $data); 
   }
}
?>