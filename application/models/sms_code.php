<?php
Class Sms_code extends CI_Model
{
   function get_sms_code_for_id($sms_code_id) 
   {
      $this -> db -> from('sms_code');
      $this -> db -> where('id', $sms_code_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }
    
   function get_sms_code($hospital_id, $doctor_id){
      $this -> db -> from('sms_code');
      $this -> db -> where('doctor_id', $doctor_id);
      $this -> db -> where('hospital_id', $hospital_id);
      $this -> db -> where('status', 1);
      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }

   function get_sms_codes_for_hospital($hospital_id){
      $this -> db -> from('sms_code');
      $this -> db -> where('hospital_id', $hospital_id);
      $this -> db -> where('status', 1);
      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
         return $query->result();
      }
      else
      {
         return false;
      }
   }


   function get_hospital_doctor_sms_code($sms_code) 
   {
      $this -> db -> select('sms_code.id, doctor.display_name as doctor_name, hospital.name as hospital_name');
      $this -> db -> from('sms_code, hospital, doctor');
      $this -> db -> where('sms_code.hospital_id = hospital.id');
      $this -> db -> where('sms_code.doctor_id = doctor.id');
      $this -> db -> where('code', $sms_code);
      $this -> db -> where('status', 1);
      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }
}
?>