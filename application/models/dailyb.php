<?php

class Dailyb extends CI_Model
{
  function drop_table()
    {
        $this->db->query('DROP TABLE IF EXISTS `dailyb` ;');
    }

    function create_table()
    {
        $this->db->query('
        CREATE TABLE IF NOT EXISTS `dailyb` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `date` DATETIME NOT NULL,
        `nappo` INT NOT NULL DEFAULT 0,
        `dailyq_id` INT NOT NULL,
        `updated` DATETIME NULL,
        PRIMARY KEY (`id`),
        INDEX `fk_dailyb_dailyq1_idx` (`dailyq_id` ASC),
        CONSTRAINT `fk_dailyb_dailyq1`
          FOREIGN KEY (`dailyq_id`)
          REFERENCES `dailyq` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION)
      ENGINE = InnoDB;
      ');
    }

    //this function fills the table using dailyq 
    function fill_table() 
    {
      //read dailyq table that sms_code has bookings
      $this -> db -> select('dailyq.id');
      $this -> db -> from('dailyq, sms_code');
      $this -> db -> where('dailyq.sms_code_id = sms_code.id');
      $this -> db -> where('sms_code.booking_enabled', 1);
      $query = $this -> db -> get();
      foreach ($query->result() as $row)
      {
        $dailyq_id = $row->id;
        $data = array(
            'dailyq_id' => $dailyq_id,
            'date' => get_current_date(),
            'updated' => get_current_date_time()
          );
        $this->db->insert('dailyb', $data); 
      }
    }

    function insert_booking($dailyq_id, $type)
    {
      // $this->db->select('id, nappo');
      // $this->db->from('daily_booking');
      // $this->db->where('dailyq_id', $dailyq_id);
      // $query = $this->db->get();

      // $id = $query->row()->id;
      // $nappo = $query->row()->nappo + 1;

      // $data = array(
      //     'nappo' => $nappo,
      //     'updated' => get_current_date_time()
      //     );
      // $this->db->where('id', $id);
      // $this->db->update('dailyb', $data); 

      // //insert to booking record
      // $data = array(
      //     'dailyb_id' => $id,
      //     'appo' => $nappo,
      //     'type' => $type,
      //     'time_stamp' => get_current_date_time()
      //     );
      // $this->db->trans_start();
      // $this->db->insert('booking_record',$data);
      // $this->db->trans_complete();
      // return  $nappo;
    }

    function add_booking($dailyb_id)
    {
      $this->db->trans_start();
      
      $this->db->select('nappo');
      $this->db->from('dailyb');
      $this->db->where('id', $dailyb_id);
      $nappo = $this->db->get()->row()->nappo;

      $nappo = $nappo + 1;

      $data = array(
          'nappo' => $nappo,
          'updated' => get_current_date_time()
          );
      $this->db->where('id', $dailyb_id);
      $this->db->update('dailyb', $data);

      $this->db->trans_complete();
      return $nappo;
    }

    function get_dailyb_by_dailyq_id($dailyq_id)
    {
      $this->db->select('dailyb.id, dailyb.nappo');
      $this->db->from('dailyb, dailyq');
      $this->db->where('dailyb.dailyq_id = dailyq.id');
      $this->db->where('dailyq.id', $dailyq_id);
      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }

    function get_dailyq_by_dailyb_id($dailyb_id)
    {
      $this->db->select('dailyq.id, dailyq.shift_type, dailyq.shift_special_id, dailyq.shift_regular_id');
      $this->db-> from('dailyq, dailyb');
      $this->db->where('dailyb.dailyq_id = dailyq.id');
      $this->db-> where('dailyb.id', $dailyb_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }


    function get_bookings()
    {
      //read dailyq table that sms_code has bookings
      $this -> db -> select('hospital.name as hospital_name, doctor.display_name as doctor_display_name, dailyb.nappo as nappo');
      $this -> db -> from('dailyb, dailyq, sms_code, doctor, hospital');
      $this -> db -> where('dailyb.dailyq_id = dailyq.id');
      $this -> db -> where('dailyq.sms_code_id = sms_code.id');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $query = $this -> db -> get();
      return $query->result();
    }

    function get_date_for_ucode()
    {
      $this -> db -> select("DATE_FORMAT(dailyb.date, '%Y%m%d') as date", FALSE);
      $this -> db -> from("dailyb");
      $this -> db -> group_by("date");

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }

    function get_active_bookings_for_hospital($hospital_id)
    {
      $this -> db -> select("doctor.id as doctor_id, doctor.display_name as doctor_display_name, dailyq.patient_count as seeing_patient, 
                            dailyb.id as booking_id, dailyb.nappo as nappo, dailyq.status as que_status, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, 
                            DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
      $this -> db -> from('hospital, doctor, sms_code, dailyq, dailyb');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('dailyq.sms_code_id = sms_code.id');
      $this -> db -> where('dailyb.dailyq_id = dailyq.id');
      $this -> db -> where('dailyq.status != 4');//cancelled shifts
      $this -> db -> where('hospital.id', $hospital_id);
      $this -> db -> order_by("doctor_display_name", "asc");
      $this -> db -> order_by("start_time", "asc");
      $query = $this -> db -> get();
      return $query->result_array();
    }

    // function get_seeing_patient($dailyb_id)
    // {
    //   $this->db->select('dailyq.patient_count as seeing_patient');
    //   $this->db->from('dailyb, dailyq');
    //   $this->db->where('dailyb.dailyq_id = dailyq.id');
    //   $this->db->where('dailyb.id', $dailyb_id);
    //   $seeing_patient = $this->db->get()->row()->seeing_patient;
    //   return $seeing_patient;
    // }

    function get_que_booking_status($dailyb_id)
    {
      $this->db->select('dailyq.status as que_status, dailyq.patient_count as seeing_patient, dailyb.nappo as nappo');
      $this->db->from('dailyb, dailyq');
      $this->db->where('dailyb.dailyq_id = dailyq.id');
      $this->db->where('dailyb.id', $dailyb_id);
      $seeing_patient = $this->db->get()->row();
      return $seeing_patient;
    }

}
?>