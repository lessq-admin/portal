<?php
Class Doctor extends CI_Model
{
   function get_doctors_for_hospital($hospital_id){
      $this -> db -> select("doctor.id as doctor_id, 
                             doctor.display_name as display_name");
      $this -> db -> from('doctor, sms_code, hospital');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('hospital.id', $hospital_id);
      $this -> db -> where('sms_code.status', 1);
      $doctor_query = $this -> db -> get();
      if($doctor_query  -> num_rows() > 0)
      {
         return $doctor_query->result();
      }
      else
      {
         return false;
      }
   }

   function get_doctor($doctor_id)
   {
      $this -> db -> select('doctor.id as doctor_id, doctor.display_name as display_name', FALSE);
      $this -> db -> from('doctor doctor');
      $this -> db -> where('doctor.id', $doctor_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }

   function get_doctor_info($doctor_id) 
   {
      $this -> db -> from('doctor');
      $this -> db -> where('doctor.id', $doctor_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }
}
?>