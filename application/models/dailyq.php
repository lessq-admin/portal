<?php

class Dailyq extends CI_Model
{
    /*
    Status field of the dailyq
    0: (default) not stated
    1: started
    2: finish
    3: eat
    4: cancel
    5: standby
    6: dissabled
    7: (default) not stated with message


    100: finish//TODo remove this



    */
    function drop_table()
    {
        $this->db->query('DROP TABLE IF EXISTS `dailyq` ;');
    }

    function create_table()
    {
        $this->db->query('
        CREATE TABLE IF NOT EXISTS `dailyq` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `date` DATETIME NULL,
        `status` INT NULL DEFAULT 0,
        `shift_type` INT NULL DEFAULT 0,
        `patient_count` INT NULL DEFAULT 0,
        `patient_in` INT NULL DEFAULT 0,
        `patient_numbers` VARCHAR(1000) NULL DEFAULT "",
        `room_number` VARCHAR(10) NULL DEFAULT "",
        `doc_app_configs` VARCHAR(500) NULL DEFAULT "",
        `special_msg` VARCHAR(45) NULL,
        `started` DATETIME NULL,
        `sms_code_id` INT NOT NULL,
        `start_time` DATETIME NULL,
        `end_time` DATETIME NULL,
        `updated` DATETIME NULL,
        `shift_special_id` INT NULL,
        `shift_regular_id` INT NULL,
        PRIMARY KEY (`id`),
        INDEX `fk_dailyq_sms_code1_idx` (`sms_code_id` ASC),
        INDEX `fk_dailyq_shift_special1_idx` (`shift_special_id` ASC),
        INDEX `fk_dailyq_shift_regular1_idx` (`shift_regular_id` ASC),
        CONSTRAINT `fk_dailyq_sms_code1`
          FOREIGN KEY (`sms_code_id`)
          REFERENCES `sms_code` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION,
        CONSTRAINT `fk_dailyq_shift_special1`
          FOREIGN KEY (`shift_special_id`)
          REFERENCES `shift_special` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION,
        CONSTRAINT `fk_dailyq_shift_regular1`
          FOREIGN KEY (`shift_regular_id`)
          REFERENCES `shift_regular` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION)
      ENGINE = InnoDB;

        ');
    }

    //this function fills the table using shift and shift_special
    function fill_table() 
    {
        $date = new DateTime();
        $dayofweek = $date->format('w');
        $fdate = $date->format('Y-m-d 00:00:00');
        // return $fdate;

        //PROCESSING FOR WEEKLY SHIFT TABLE
        //get datafrom shift
        $this -> db -> select("sms_code.id as sms_code_id, 
                               shift_regular.id as shift_regular_id,
                               shift_regular.start_time as start_time,
                               shift_regular.end_time as end_time");
        $this -> db -> from('shift_regular, sms_code');
        $this -> db -> where('shift_regular.sms_code_id = sms_code.id');
        $this -> db -> where('shift_regular.status', 1);
        $this -> db -> where('shift_regular.day', $dayofweek);
        $query = $this -> db -> get();
        // return $fdate;
        // return $query -> num_rows();

        foreach ($query->result() as $row)
        {
          $sms_code_id = $row->sms_code_id;
          $shift_regular_id = $row->shift_regular_id;
          $start_time = $row->start_time;
          $end_time = $row->end_time;
          $dailyq_status = 0;
          //check the shift is cancel or not
          $this -> db -> select("shift_cancel.id, shift_cancel.status");
          $this -> db -> from("shift_regular, shift_cancel");
          $this -> db -> where("shift_regular.id = shift_cancel.shift_regular_id");
          $this -> db -> where("shift_cancel.status != 0");
          $this -> db -> where("shift_cancel.date", $fdate);
          $this -> db -> where("shift_regular.id", $shift_regular_id);
          $cancel_shift_query = $this -> db -> get();
          // if cancelled update the statu of shift_cancel
          if($cancel_shift_query  -> num_rows() == 1)
          {
            $cancel_shift = $cancel_shift_query->row();
            //if cancel stats equals 2->dissable the queue
            if($cancel_shift->status == 1)
              $dailyq_status = 4;
            else if($cancel_shift->status == 2)
              $dailyq_status = 6;
            //update cancel status to 0
            $data = array(
               'status' => 0,
               'updated' => get_current_date_time()
            );
            $this->db->where('id', $cancel_shift->id);
            $this->db->update('shift_cancel', $data); 
          }

          $data = array(
            'sms_code_id' => $sms_code_id,
            'shift_regular_id' => $shift_regular_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'date' => get_current_date(),
            'updated' => get_current_date_time(),
            'status' => $dailyq_status,
            'shift_type' => 1
          );
          $this->db->insert('dailyq', $data); 
        }

        //PROCESSING FOR SPECIAL SHIFT TABLE
        //get datafrom shift
        $this -> db -> select("sms_code.id as sms_code_id, 
                               shift_special.id as shift_special_id,
                               shift_special.start_time as start_time,
                               shift_special.end_time as end_time");
        $this -> db -> from('shift_special, sms_code');
        $this -> db -> where('shift_special.sms_code_id = sms_code.id');
        $this -> db -> where('shift_special.status', 1);
        $this -> db -> where('shift_special.date', $fdate);
        $query = $this -> db -> get();
        foreach ($query->result() as $row)
        {
          $sms_code_id = $row->sms_code_id;
          $shift_special_id = $row->shift_special_id;
          $start_time = $row->start_time;
          $end_time = $row->end_time;
          $dailyq_status = 0;

          $data = array(
            'sms_code_id' => $sms_code_id,
            'shift_special_id' => $shift_special_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'date' => get_current_date(),
            'updated' => get_current_date_time(),
            'status' => $dailyq_status,
            'shift_type' => 2 //from special table
          );
          $this->db->insert('dailyq', $data); 
        }
    }

    public function get_dailyq($queue_id)
    {
        $this -> db -> from('dailyq');
        $this -> db -> where('id', $queue_id);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    //get all daily queues
    public function get_dailyqs()
    {
        $this -> db -> from('dailyq');
        $query = $this -> db -> get();
        if($query -> num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function update_queue_count($dailyq_id, $qcount)
    {
        $data = array(
          'patient_count' => $qcount,
          'updated' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data); 
    }

    public function update_queue_status($dailyq_id, $qcount, $patint_in, $patient_numbers)
    {
        $data = array(
          'patient_count' => $qcount,
          'patient_in' => $patint_in,
          'patient_numbers' => $patient_numbers,
          'updated' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data); 
    }

    public function get_patient_count($dailyq_id)
    {
        $this -> db -> select('patient_count');
        $this -> db -> from('dailyq');
        $this -> db -> where('id', $dailyq_id);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->row()->patient_count;
        }
        else
        {
            return -1;
        }
    }

    public function start_queue($dailyq_id)
    {
        $data = array(
          'status' => '1',
          'updated' => get_current_date_time(),
          'started' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data); 
    }

    public function finish_queue($dailyq_id)
    {
        $data = array(
          'status' => '2',
          'updated' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data);
        return $this->db->affected_rows(); 
    }

    public function reset_queue($dailyq_id)
    {
        $data = array(
          'status' => '0',
          'patient_count' => '0',
          'patient_in' => '0',
          'patient_numbers' => '',
          'room_number' => '',
          'doc_app_configs' => '',
          'updated' => get_current_date_time(),
          'started' => null
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data); 
        return $this->db->affected_rows();
    }

    function update_patient_numbers($dailyq_id, $patient_numbers)
    {
      $data = array(
          'patient_numbers' => $patient_numbers,
          'updated' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data);
        return $this->db->affected_rows();
    }

    public function get_queues_for_smscode($sms_code_id) {
      $this -> db -> select("dailyq.id, dailyq.status, dailyq.patient_count, DATE_FORMAT(dailyq.updated, '%H:%i') as updated, DATE_FORMAT(dailyq.started, '%H:%i') as started, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
      $this -> db -> from(' dailyq');
      $this -> db -> where('dailyq.sms_code_id', $sms_code_id);
      $this -> db -> where('dailyq.status != 4');//cancel shift
      $this -> db -> order_by("start_time", "asc");

      $query = $this -> db -> get();

      if($query -> num_rows() > 0)
      {
       return $query->result();
      }
      else
      {
       return false;
      }
    }

    public function get_queues_for_hospital($hospital_id) {
      $this -> db -> select("doctor.display_name, dailyq.status, dailyq.patient_count, dailyq.patient_in, dailyq.patient_numbers, DATE_FORMAT(dailyq.updated, '%H:%i') as updated, DATE_FORMAT(dailyq.started, '%H:%i') as started, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
      $this -> db -> from(' dailyq, sms_code, hospital, doctor');
      $this -> db -> where('dailyq.sms_code_id  = sms_code.id');
      $this -> db -> where('sms_code.doctor_id  = doctor.id');
      $this -> db -> where('sms_code.hospital_id', $hospital_id);
      $this -> db -> where('dailyq.status != 4');//cancel shift
      $this -> db -> order_by("display_name", "asc");
      $this -> db -> order_by("start_time", "asc");

      $query = $this -> db -> distinct()->get();

      return $query->result();
    }

    public function get_queues_for_hospital_room($hospital_id, $room_number) {
      $this -> db -> select("doctor.display_name, dailyq.status, dailyq.patient_count, dailyq.patient_in, dailyq.patient_numbers, DATE_FORMAT(dailyq.updated, '%H:%i') as updated, DATE_FORMAT(dailyq.started, '%H:%i') as started, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
      $this -> db -> from(' dailyq, sms_code, hospital, doctor');
      $this -> db -> where('dailyq.sms_code_id  = sms_code.id');
      $this -> db -> where('sms_code.doctor_id  = doctor.id');
      $this -> db -> where('sms_code.hospital_id', $hospital_id);
      $this -> db -> where('dailyq.room_number', $room_number);
      $this -> db -> where('dailyq.status != 4');//cancel shift
      $this -> db -> where('dailyq.status != 2');//finish shift
      $this -> db -> order_by("display_name", "asc");
      $this -> db -> order_by("start_time", "asc");

      $query = $this -> db -> distinct()->get();

      return $query->result();
    }

    //may be removed, check
    public function get_active_dailyq_for_hospital($hospital_id)
    {
      $this -> db -> select("doctor.id as doctor_id, doctor.display_name as doctor_display_name, 
                            dailyq.id as que_id, DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, 
                            DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time", FALSE);
      $this -> db -> from('hospital, doctor, sms_code, dailyq');
      $this -> db -> where('hospital.id = sms_code.hospital_id');
      $this -> db -> where('doctor.id = sms_code.doctor_id');
      $this -> db -> where('dailyq.sms_code_id = sms_code.id');
      $this -> db -> where('dailyq.status != 4');//cancelled shifts
      $this -> db -> where('hospital.id', $hospital_id);
      $this -> db -> order_by("doctor_display_name", "asc");
      $this -> db -> order_by("start_time", "asc");
      $query = $this -> db -> get();
      return $query->result_array();
    }

    function cancel_regular_shift($shift_regular_id)
    {
      $data = array(
         'status' => 4,
         'updated' => get_current_date_time()
       );
      $this->db->where('shift_regular_id', $shift_regular_id);
      $this->db->update('dailyq', $data);
    }

    function cancel_special_shift($shift_special_id)
    {
      $data = array(
         'status' => 4,
         'updated' => get_current_date_time()
       );
      $this->db->where('shift_special_id', $shift_special_id);
      $this->db->update('dailyq', $data);
    }

    function dissable_queue($id)
    {
      $data = array(
         'status' => 6,
         'updated' => get_current_date_time()
       );
      $this->db->where('id', $id);
      $this->db->update('dailyq', $data);
    }

    function add_special_shift($shift_special_id, $date, $start_time, $end_time, $sms_code_id, 
      $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id)
    {
      $data = array(
        'sms_code_id' => $sms_code_id,
        'shift_special_id' => $shift_special_id,
        'start_time' => $start_time,
        'end_time' => $end_time,
        'date' => get_current_date(),
        'updated' => get_current_date_time(),
        'status' => 0, //default stateus
        'shift_type' => 2, //from special table
        'booking_enabled' => $booking_enabled,
        'booking_start_time' => $booking_start_time,
        'booking_end_time' => $booking_end_time,
        'booking_config_id' => $booking_config_id,
      );
      $this->db->insert('dailyq', $data); 
    }

    function get_date_for_ucode()
    {
      $this -> db -> select("DATE_FORMAT(dailyq.date, '%Y%m%d') as date", FALSE);
      $this -> db -> from("dailyq");
      $this -> db -> group_by("date");

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }

    function standby_queue($dailyq_id, $room_number, $config_params)
    {
      $data = array(
          'status' => '0', //TODO: move to 5
          'room_number' => $room_number,
          'doc_app_configs' => $config_params,
          'updated' => get_current_date_time(),
          'started' => get_current_date_time()
          );
        $this->db->where('id', $dailyq_id);
        $this->db->update('dailyq', $data); 
    }

    function queue_for_shift_special($shift_special_id) 
    {
      $this -> db -> from("dailyq");
      $this->db->where('shift_special_id', $shift_special_id);
      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }

    function queue_for_shift_regular($shift_regular_id) 
    {
      $this -> db -> from("dailyq");
      $this->db->where('shift_regular_id', $shift_regular_id);
      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          return $query->row();
      }
      else
      {
          return false;
      }
    }

   public function get_queues_for_hospital_doctor($hospital_id, $doctor_id) {
      $this -> db -> select("
      doctor.display_name, 
      dailyq.id, 
      dailyq.status, 
      dailyq.special_msg,
      dailyq.patient_count, 
      dailyq.patient_in, 
      dailyq.patient_numbers, 
      DATE_FORMAT(dailyq.updated, '%H:%i') as updated, 
      DATE_FORMAT(dailyq.started, '%H:%i') as started, 
      DATE_FORMAT(dailyq.start_time, '%H:%i') as start_time, 
      DATE_FORMAT(dailyq.end_time, '%H:%i') as end_time, 
      dailyq.shift_regular_id,
      dailyq.shift_special_id,
      dailyq.booking_enabled,
      dailyq.booking_start_time,
      dailyq.booking_end_time,
      dailyq.booking_config_id", FALSE);
      $this -> db -> from(' dailyq, sms_code, hospital, doctor');
      $this -> db -> where('dailyq.sms_code_id  = sms_code.id');
      $this -> db -> where('sms_code.doctor_id  = doctor.id');
      $this -> db -> where('sms_code.hospital_id', $hospital_id);
      if($doctor_id)
        $this -> db -> where('doctor.id', $doctor_id);
      $this -> db -> order_by("display_name", "asc");
      $this -> db -> order_by("start_time", "asc");

      $query = $this -> db -> distinct()->get();

      return $query->result();
    }

}
?>