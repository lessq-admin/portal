<?php
Class Hospital extends CI_Model
{
   function get_hospital($hospital_id)
   {
      $this -> db -> select('hospital.id as hospital_id, hospital.name as hospital_name, hospital.type as type', FALSE);
      $this -> db -> from('hospital hospital');
      $this -> db -> where('hospital.id', $hospital_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }

   function get_hospital_info($hospital_id)
   {
      $this -> db -> from('hospital');
      $this -> db -> where('hospital.id', $hospital_id);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
         return $query->row();
      }
      else
      {
         return false;
      }
   }
   
   // function populate_dropdown(){
   //    $this->db->from('hospital');
   //    $this->db->order_by('name');
   //    $result = $this->db->get();
   //    $return = array();
   //    $return[''] = '--Select--'; 
   //    if($result->num_rows() > 0) {
   //       foreach($result->result_array() as $row) {
   //          $return[$row['id']] = $row['name'];
   //       }
   //    }

   //    return $return;
   // }

   // function populate_dropdown_all(){
   //    $this->db->from('hospital');
   //    $this->db->order_by('name');
   //    $result = $this->db->get();
   //    $return = array();
   //    $return['0'] = '--All--'; 
   //    if($result->num_rows() > 0) {
   //       foreach($result->result_array() as $row) {
   //          $return[$row['id']] = $row['name'];
   //       }
   //    }

   //    return $return;
   // }

   function get_all_hospitals(){
      $this->db->from('hospital');
      $this->db->order_by('name');
      $hospital_query = $this -> db -> get();
      if($hospital_query  -> num_rows() > 0)
      {
         return $hospital_query->result();
      }
      else
      {
         return false;
      }
   }
}
?>