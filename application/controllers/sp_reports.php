<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_reports extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}

   		$this->load->model('dailyq','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('shift','',TRUE);
   		$this->load->model('sms_log','',TRUE);
   		$this->load->model('dailyq_log','',TRUE);
   		$this->load->model('doctor','',TRUE);
   		$this->load->model('sms_code','',TRUE);
   		$this->load->model('device','', TRUE);
   		$this->load->model('dailyb','', TRUE);
   		$this->load->model('dailyb_log','', TRUE);
 	}

 	public function index()
	{
		if(getenv('APPLICATION_ENV') == 'production')
        {
            $title = 'Reports';
        }
        else if(getenv('APPLICATION_ENV') == 'staging')
        {
            $title = 'Reports-Staging';
        }
        else 
        {
            $title = 'Reports-Local';
        }
        $data['title'] = $title;
		$this->load->view('sp_reports', $data);
	}

  public function hospital_timetable()
  {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--Select--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $this->load->view('hospital_timetable', $data);
  }

  function q_operation_chart(){
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--Select--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $currdate = date('Y-m-d', time());
    $pre_date = date('Y-m-d', strtotime($currdate .' -1 day'));
    $data['pre_date'] = $pre_date;
    $this->load->view('sp_q_operation_chart', $data);
  }

  function json_q_operation_chart()
  {
    $log_data = array();
    $hospital_id = $this->input->post('hospital_id');
    $date = $this->input->post('date');

    $shift_regular_ids = $this->dailyq_log->get_regular_shift_ids_for_hospital_date($hospital_id, $date);

    $shift_special_ids = $this->dailyq_log->get_special_shift_ids_for_hospital_date($hospital_id, $date);

    if($shift_regular_ids || $shift_special_ids)
    {
      if($shift_regular_ids)
      {
        foreach ($shift_regular_ids as $key => $val)
        {
          $record = array();

          $shift = $this->shift->get_regular_shift($val->shift_regular_id);
          $record["shift_type"] = 'Regular';
          $record["doctor_name"] = $shift->display_name;
          $record["date"] = $date;
          $record["start_time"] = $shift->start_time;
          $record["end_time"] = $shift->end_time;

          $patient_cnt_logs = $this->dailyq_log->get_shift_regular_logs($shift->shift_id, $date);

          $table = array();
          $table['cols'] = array(
            array('label' => 'time', 'type' => 'string'),
              array('label' => 'Patients', 'type' => 'number'),
              array('type' => 'string', 'role' => 'annotation')
          );

          $rows = array();
          foreach ($patient_cnt_logs as $key => $val) 
          {
            $temp = array();
                $temp[] = array('v' => $val->time_stamp); 
                $temp[] = array('v' => $val->patient_count); 
                if($val->msg_status == 1)
                  $temp[] = array('v' => 'Start');
                else if($val->msg_status == 3)
                  $temp[] = array('v' => 'Finish'); 
                else if($val->msg_status == 4)
                  $temp[] = array('v' => 'Reset');
                else if($val->msg_status == 100)
                  $temp[] = array('v' => 'Start Error');
                else if($val->msg_status == 200)
                  $temp[] = array('v' => 'Update Error');
                else if($val->msg_status == 300)
                  $temp[] = array('v' => 'Finish Error');
                else if($val->msg_status == 400)
                  $temp[] = array('v' => 'Reset Error');
                
                $rows[] = array('c' => $temp);
            }
            $table['rows'] = $rows;
            $record["table"] = $table;
            $log_data[] = $record;
        }
      }
      //special shifts
      if($shift_special_ids)
      {
        foreach ($shift_special_ids as $key => $val)
        {
          $record = array();

          $shift = $this->shift->get_special_shift($val->shift_special_id);
          $record["shift_type"] = 'Special';
          $record["doctor_name"] = $shift->display_name;
          $record["date"] = $date;
          $record["start_time"] = $shift->start_time;
          $record["end_time"] = $shift->end_time;

          $patient_cnt_logs = $this->dailyq_log->get_shift_special_logs($shift->shift_id, $date);

          $table = array();
          $table['cols'] = array(
            array('label' => 'time', 'type' => 'string'),
              array('label' => 'Patients', 'type' => 'number'),
              array('type' => 'string', 'role' => 'annotation')
          );

          $rows = array();
          foreach ($patient_cnt_logs as $key => $val) 
          {
            $temp = array();
                $temp[] = array('v' => $val->time_stamp); 
                $temp[] = array('v' => $val->patient_count); 
                if($val->msg_status == 1)
                  $temp[] = array('v' => 'Start');
                else if($val->msg_status == 3)
                  $temp[] = array('v' => 'Finish'); 
                else if($val->msg_status == 4)
                  $temp[] = array('v' => 'Reset');
                else if($val->msg_status == 100)
                  $temp[] = array('v' => 'Start Error');
                else if($val->msg_status == 200)
                  $temp[] = array('v' => 'Update Error');
                else if($val->msg_status == 300)
                  $temp[] = array('v' => 'Finish Error');
                else if($val->msg_status == 400)
                  $temp[] = array('v' => 'Reset Error');
                $rows[] = array('c' => $temp);
            }
            $table['rows'] = $rows;
            $record["table"] = $table;
            $log_data[] = $record;
        }
      }
    }
    else
    {
      $log_data["error"] = "No any queues operated on the selected date";
    }
    // print_r($log_data);
    $json_data= json_encode($log_data);
    echo $json_data;
  }

  public function daily_sms_count()
  {
    $data["logs"] = $this->sms_log->get_sms_log();
    $this->load->view('show_sms_log', $data);
  }

  public function subscriber_sms_count()
  {
    $min_date = $this->sms_log->get_min_date();
    if(!$min_date)
      $data['min_date'] = date('Y-m-d', time());
    else
      $data['min_date'] = $min_date;
    $data['max_date'] = date('Y-m-d', time());
    $this->load->view('sp_subscriber_sms_count', $data);
  }

  public function subscriber_sms_count_result()
  {
    $hospital_id = $this->input->post('ddl_hospitals');
    $from_date = $this->input->post('from_date');
    $to_date = $this->input->post('to_date');
    $res = $this->sms_log->get_sms_for_dates($from_date, $to_date);
    
    $add_arr = array();
    $data_arr = array();
    foreach ($res as $key => $val)
    {
      $request = $val->request;
      $obj = json_decode($request);
      $add_arr[] = $obj->sourceAddress;
      $row['id'] = $val->id;
      $row['carrier'] = $val->carrier;
      $row['tel'] = $obj->sourceAddress;

      array_push($data_arr, $row);
    }
    $add_arr_cnt = array_count_values($add_arr);

    //iterating and making display arr
    $display_arr = array();
    $cnt = 1;
    $tot_sms = 0;
    foreach ($add_arr_cnt as $key => $val)
    {
      $carrier = '';
      foreach ($data_arr as $key1 => $val1)
      {
        if ($val1['tel'] === $key) {
          $carrier = $val1['carrier'];
            }
      }
      $drow = array(
      'no' => $cnt++,
      'tel' => $key,
      'cnt' => $val,
      'carrier' => $carrier);
      $tot_sms += intval($val);
      // array_push($display_arr, $drow);
      $display_arr[] = (object)$drow;
      // print_r($drow);
    }
    $data["tbl_data"] = $display_arr;
    $data["from_date"] = $from_date;
    $data["to_date"] = $to_date;
    $data["tot_sms"] = $tot_sms;
    $data["nusers"] = count($add_arr_cnt);
    $this->load->view('sp_subscriber_sms_count_results', $data);

  }


	/*************************** End Region AJAX calls ***********************/




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */