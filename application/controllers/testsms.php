<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Testsms extends CI_Controller {
 
    public function index()
    {

        $sourceAddress = "chamidu host";
        $message = "lessq 1200";
        $requestId = "00121122";
        $applicationId = "883321234434";
        $encoding = "iso11";
        $version = "v0.1";

        $arrayField = array(
            "sourceAddress" => $sourceAddress,
            "message" => $message,
            "requestId" => $requestId,
            "applicationId" => $applicationId,
            "encoding" => $encoding,
            "version" => $version);
        $jsonObjectFields = json_encode($arrayField);

        $senderURL = "http://localhost:8888/lessq/sms/sms_test";
        $ch = curl_init($senderURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        echo $res;
    }

    public function urltest()
    {
        echo 'received';
        $inrec = file_get_contents('php://input');
        $array = json_decode($inrec, true);
        $message = $array['message'];
        echo $message;
    }

    public function testme()
    {
        $inrec = file_get_contents('php://input');
        $array = json_decode($inrec, true);
        $sourceAddress = $array['message'];
        $smsparts = preg_split('/\s+/', $sourceAddress);
        print_r($smsparts);
    }

    public function testme2($phone)
    {
        if (ctype_digit ($phone) && strlen($phone) == 10 && substr($phone, 0, 2) == '07') 
        {
            echo $phone;
        }
        else
            echo 'invalid';
    }
}
 
/* End of file Blog.php */
/* Location: ./application/controllers/blog.php */