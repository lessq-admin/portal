<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qmonitor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!isset($_SERVER['PHP_AUTH_USER']) || 
		$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
		$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
		{
			header('WWW-Authenticate: Basic realm="Admin"');
			header('HTTP/1.0 401 Unauthorized');
			die('Access Denied');
		}

		$this->load->model('dailyq','',TRUE);
		$this->load->model('notification','', TRUE);

		define('QUPDATE_CHECK_TIME', '900');//15 minuites 900
	}
	// public function index()
	// {
	// 	echo SENDER_URL_DIALOG;
	// 	echo getenv('APPLICATION_ENV');
	// 	echo "Hi thre" ;
	// 	$this->load->helper('utils_helper');
	// 	echo get_current_date_time();
	// 	echo test_method('Hello World');
	// }

	function monitor()
	{
		// if(getenv('APPLICATION_ENV') != 'staging')
		// 	return;
		$queus = $this->dailyq->get_dailyqs();
		// print_r($queus);
		foreach($queus as $que)
		{
			//check status for started
			if($que->status == 1) //check update time
			{
				$update_time = strtotime($que->updated);
				$curr_time = strtotime(get_current_date_time());
				// echo $que->updated.'</br>'.get_current_date_time().'</br>';
				// $diff = round(abs($curr_time - $update_time) / 60,2);
				// echo $diff;
				$diff = $curr_time - $update_time;
				if($diff > QUPDATE_CHECK_TIME)
				{
					// echo $diff;
					$pre_notification = $this->notification->get_today_notification_for_queue_id($que->id);
					// print_r($pre_notification) ;
					if($pre_notification)
					{
						$last_notification_time = strtotime($pre_notification->time_stamp);
						$notification_diff = $curr_time - $last_notification_time;

						if($notification_diff <= QUPDATE_CHECK_TIME)
						{
							continue;
						}
						else
						{
							$this->notification->update_notification_status($pre_notification->id, 0);
						}
					}
						
					$qinfo = $this->notification->get_qdata_for_notification($que->id);
					$hospital_name = $qinfo->hospital_name;
					$doctor_display_name = $qinfo->doctor_display_name;
					$start_time = $qinfo->start_time;
					$end_time = $qinfo->end_time;
					$notification = "LessQ Alert\n".
						"Q is not updated for last ".round(abs($diff) / 60, 0)." minuites\n".
						$hospital_name."\n".
						$doctor_display_name."\n".
						"shift: ".$start_time." to ".$end_time;
					
					//adding to qmonitor
					$this->notification->add_notification($que->id, $notification, 1);
					
					//dialog parameters
					$senderURL = 'https://api.dialog.lk/sms/send';
					$applicationId = "APP_009919";
					$password = "760daa5b4cdb455789162e8cdd1f162e";

					$senders = array(
						"tel:AZ110kXwfAVMcutJQ+NPcb/yiBE9W9hyfpdiMA2/Zl52y++f1uT7I6vQOGc1ZSzhLboMO", //rukshan
						"tel:B%3C4/6WkyA98Yy7+PV2RtntYp2geCqU6DWuTXy3FC7I+kMOyFHUr7eYrfMkR7ssopKR8"  //vidura
						);

					$arrayField = array("applicationId" => $applicationId,
			            "password" => $password,
			            "message" => $notification,
			            "destinationAddresses" => $senders);
			        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);

			        $ch = curl_init($senderURL);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_POST, 1);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        $res = curl_exec($ch);
			        // echo $res;
			        curl_close($ch);
				}
					
			}
		}
		
	}

}