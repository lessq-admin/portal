<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Fileupload extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        // error_reporting(E_ALL | E_STRICT);
        // $this->load->library("uploadhandler");
        $this->load->view('fupload', array('error' => ''));
    }

    public function do_upload()
	{
	    $this->load->library("uploadhandler");
	}

	public function do_delete()
	{
	    $this->load->library("uploadhandler");
	}
}