<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_pdu extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}

   		$this->load->model('pdu','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('doctor','',TRUE);
 	}

 	public function index()
	{
		if(getenv('APPLICATION_ENV') == 'production')
    {
        $title = 'PDUs';
    }
    else if(getenv('APPLICATION_ENV') == 'staging')
    {
        $title = 'PDUs-Staging';
    }
    else 
    {
        $title = 'PDUs-Local';
    }
    $data['title'] = $title;
    $pdus = $this->pdu->get_pdus();
    $data['pdus'] = $pdus;
    //print_r($pdus);
		$this->load->view('sp_pdu', $data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */