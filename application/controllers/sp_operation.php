<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_operation extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
      if (!isset($_SERVER['PHP_AUTH_USER']) || 
        $_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        $_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
      {
        header('WWW-Authenticate: Basic realm="Admin"');
        header('HTTP/1.0 401 Unauthorized');
        die('Access Denied');
    }

    $this->load->model('dailyq','',TRUE);
    $this->load->model('hospital','',TRUE);
    $this->load->model('shift','',TRUE);
    $this->load->model('sms_log','',TRUE);
    $this->load->model('dailyq_log','',TRUE);
    $this->load->model('doctor','',TRUE);
    $this->load->model('sms_code','',TRUE);
    $this->load->model('device','', TRUE);
    $this->load->model('dailyb','', TRUE);
    $this->load->model('dailyb_log','', TRUE);
 	}

  /************************************************************************/
  /*************************** Region UI calls ****************************/
  /************************************************************************/
  public function index()
  {
    //read flash data
    if($this->session->flashdata('msg'))
      $data['msg'] = $this->session->flashdata('msg');

    $data['title'] = $this->get_title();

    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list = array();
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    //sttus
    $status_list['0'] = 'Inactive';
    $status_list['1'] = 'Active';

    //get device information
    $device = $this->device->get_operational_device();
    $data['ddl_hospital'] = $device->hospital_id;
    $data['device_uid'] = $device->uid;
    $data['passcode'] = $device->passcode;
    $data['description'] = $device->description;
    $data['ddl_status'] = $device->status;
    $data['status_list'] = $status_list;

    $this->load->view('sp_operation', $data);
  }

  function change_operational_device_setting() {
    $txt_password = $this->input->post('txt_password');
    $hospital_id = $this->input->post('ddl_hospitals');
    $status = $this->input->post('ddl_status');

    if($txt_password == null || $txt_password != "lessqop3r@ti0n")
    {
      $this->session->set_flashdata('msg', 'Invalid password');    
      redirect(site_url('sp_operation'), 'refresh'); 
    }
    else
    {
      //update table
      $this->device->update_operational_device($hospital_id, $status);  
      $this->session->set_flashdata('msg', 'Operation suceess');    
      redirect(site_url('sp_operation'), 'refresh'); 
    }
  }

  public function test(){
    $this->load->view('tsort.php');
  }

  public function authenticate()
  {
    $data['title'] = $this->get_title();

    $txt_password = $this->input->post('txt_password');
    if($txt_password == null || $txt_password != "lessq")
    {
      $data['msg'] = 'Invalid password';
      $this->load->view('sp_core_login', $data);
    }
    else
    {
      //add login to session
      $this->load->view('sp_core', $data);
    }
  }

  public function reset_daily_qb()
  {
    $res = $this->reset_dailyqb_tables();
    $data['title'] = $this->get_title();
    $data['msg'] = $res;
    $this->load->view('sp_core', $data);
  }

  public function internal_sms_reply()
  {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--All--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $data['from_date'] = date('Y-m-d', strtotime("-1 days"));
    $data['to_date'] = date('Y-m-d', time());
    $doctor_list = array();
        $doctor_list[''] = '--All--'; 
    $data['doctor_list'] = $doctor_list;
    $sms_status_list['-1'] = '--All--'; 
    $sms_status_list['1'] = 'Success'; 
    $sms_status_list['0'] = 'Failed'; 
    $data['sms_status'] = $sms_status_list;

    $group_req_list['0'] = 'No'; 
    $group_req_list['1'] = 'Yes';  
    $data['group_req'] = $group_req_list;

    $this->load->view('sp_sms_reply', $data);
  }

  public function sms_reply_log() {
    $config =& get_config();
    $log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';

    $logs = scandir($log_path);
    $sms_logs = array();
    foreach ($logs as $key => $value) 
    { 
      if (false !== strpos($value, 'log')) 
      { 
        $sms_logs[] = substr($value, 0, -4);
      } 
    } 
    $data = array();
    $data['sms_logs'] = $sms_logs;
    //send the data to page
    $this->load->view('sp_core_sms_reply_logs', $data);
  }

  public function get_log() {
    $config =& get_config();
    $log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';
    $abs_file = $log_path.$this->uri->segment(3).'.php';
    $file = @fopen($abs_file, "rb");

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.$this->uri->segment(3).'.txt');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($abs_file));
    while (!feof($file)) {
        print(@fread($file, 1024 * 8));
        ob_flush();
        flush();
    }
  }

  public function internal_sms_reply_search_results()
  {
    $hospital_id = $this->input->post('ddl_hospitals');
    $doctor_id = $this->input->post('ddl_doctors');
    $from_date = $this->input->post('from_date');
    $to_date = $this->input->post('to_date');
    $sms_status = $this->input->post('ddl_sms_status');
    $group_req = $this->input->post('ddl_group_req');

    $sms_codes = array();
    if($hospital_id != 0 && $doctor_id != 0)
    {
      $sms_code = $this->sms_code->get_sms_code($hospital_id, $doctor_id);
      $sms_codes[] = (object)$sms_code;
    }
    else if($hospital_id != 0) //doctor_id == 0
    {
      $res = $this->sms_code->get_sms_codes_for_hospital($hospital_id);
      foreach($res as $key => $sms_code)
      {
        $sms_codes[] = (object)$sms_code;
      }
    }

    $tbl_data = array();
    $all_data = array();
    $res_array = array();
    if(count($sms_codes) > 0)
    {
      foreach ($sms_codes as $key => $row)
      { 
        $sms_code = $row->code;
        $res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
        foreach ($res as $key => $row)
        {
          $res_array[] = (object)$row;
        }
      }
    }
    else
    {
      $sms_code = '';
      $res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
      foreach ($res as $key => $row)
      {
        $res_array[] = (object)$row;
      }
    }

    
    foreach ($res_array as $key => $val)
    {
      $request = $val->request;
      $response = $val->response;
      $obj_req = json_decode($request);
      $obj_res = json_decode($response);
      $req_msg = $obj_req->message; 
      $res_msg = $obj_res->message;
      $tel     = $obj_req->sourceAddress; 
      $date_time = $val->request_time;
      $carrier = $val->carrier;
      $id = $val->id;
      $row = array(
        'log_id' => $id,
        'carrier' => $carrier,
        'date_time' => $date_time,
        'tel' => $tel,
        'req_msg' => $req_msg,
        'res_msg' => $res_msg
      );
      $all_data[] = $row;
    }

    if($group_req == 1)//'yes'
    {
      //grouping
      $groups = array();
      foreach ($all_data as $item) {
          $key = $item['tel'];
          if (!isset($groups[$key])) {
              $groups[$key] = array(
                  'item' => $item,
                  'count' => 1,
              );
          } else {
              $groups[$key]['item'] = $item;
              $groups[$key]['count'] += 1;
          }
      }

      // print_r($groups);
      $cnt = 0;
      foreach ($groups as $item) {
        $req_msg = $item['item']['req_msg'];
        $res_msg = $item['item']['res_msg'];
        $tel = $item['item']['tel'];
        $tel = '...'.substr($tel, -10); 
        // if(strlen($req_msg) > 12)
        //   $req_msg = substr($req_msg, 0, 50).'...'; //
        // if(strlen($res_msg) > 30)
        //   $res_msg = substr($res_msg, 0, 90).'...'; 
        $row = array(
          'no' => $cnt++,
          'log_id' => $item['item']['log_id'],
          'carrier' => $item['item']['carrier'],
          'date_time' => $item['item']['date_time'],
          'tel' => $tel,
          'req_msg' => $req_msg,
          'res_msg' => $res_msg,
          'count' => $item['count']
        );
        $tbl_data[] = (object)$row;
      }
    }
    else {
      // print_r($all_data);
      $cnt = 0;
      foreach ($all_data as $item) {
        $req_msg = $item['req_msg'];
        $res_msg = $item['res_msg'];
        $tel = $item['tel'];
        $tel = '...'.substr($tel, -10); 
        if(strlen($req_msg) > 12)
          $req_msg = substr($req_msg, 0, 20).'...'; //
        if(strlen($res_msg) > 30)
          $res_msg = substr($res_msg, 0, 60).'...'; 
        $row = array(
          'no' => $cnt++,
          'log_id' => $item['log_id'],
          'carrier' => $item['carrier'],
          'date_time' => $item['date_time'],
          'tel' => $tel,
          'req_msg' => $req_msg,
          'res_msg' => $res_msg,
          'count' => 1
        );
        $tbl_data[] = (object)$row;
      }
    }

    //preparing display data
    if($hospital_id == 0)
      $selected_hospital = '--All--';
    else
      $selected_hospital = $this->hospital->get_hospital($hospital_id)->hospital_name;
    if($doctor_id == 0)
      $selected_doctor = '--All--';
    else
      $selected_doctor = $this->doctor->get_doctor($doctor_id)->display_name;
    $sms_status_list['-1'] = '--All--'; 
    $sms_status_list['1'] = 'Success'; 
    $sms_status_list['0'] = 'Failed'; 
    // echo $sms_status; return;
    $selected_sms_status = $sms_status_list[$sms_status];

    $group_req_list['0'] = 'No'; 
    $group_req_list['1'] = 'Yes'; 
    $selected_group_req = $group_req_list[$group_req];

    $data['selected_hospital'] = $selected_hospital;
    $data['selected_doctor'] = $selected_doctor;
    $data['selected_sms_status'] = $selected_sms_status;
    $data['selected_from_date'] = $from_date;
    $data['selected_to_date'] = $to_date;
    $data['selected_sms_status'] = $selected_sms_status;
    $data['selected_group_req'] = $selected_group_req;
    $data['tbl_data'] = $tbl_data;

    $this->load->view('sp_sms_reply_result', $data);
  }

  function internal_sms_reply_confirm()
  {
    $sms_text = $this->input->post('txt_sms');
    $check_list = $this->input->post('check_list'); 
    // echo count($check_list);
    // echo $date;
    if(!empty($check_list)) 
    {
      foreach($check_list as $log_id) 
      {
        // echo $log_id;
        $sms_log = $this->sms_log->get_sms_log_for_id($log_id);
        $carrier = $sms_log->carrier;
        $response = $sms_log->response;
        $obj_res = json_decode($response);
        if($carrier == 'dialog')
          $senderURL = SENDER_URL_DIALOG;//'https://localhost:7443/sms/send';
        if($carrier == 'etisalat')
          $senderURL = SENDER_URL_ETISALAT;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $obj_res->applicationId,
                "password" => $obj_res->password,
                "message" => $sms_text,
                "destinationAddresses" => $obj_res->destinationAddresses);
            $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
            //TODO:make log in database

            $ch = curl_init($senderURL);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            curl_close($ch);
      }
      $this->session->set_flashdata('msg', 'SMS reply completed');
    }
    else
    {
      $this->session->set_flashdata('msg', 'Error occured </br> Start from begining');
    }
    redirect(site_url('sp_core/internal_sms_reply'), 'refresh'); 
  }


  /*************************** End Region UI calls ***********************/

  /************************************************************************/
  /*************************** Region AJAX calls **************************/
  /************************************************************************/



  /*************************** End Region AJAX calls **********************/

  /************************************************************************/
  /*************************** Region Utils *******************************/
  /************************************************************************/
  private function get_title()
  {
    if(getenv('APPLICATION_ENV') == 'production')
    {
        $title = 'LessQ Operation';
    }
    else if(getenv('APPLICATION_ENV') == 'staging')
    {
        $title = 'LessQ Operation-Staging';
    }
    else 
    {
        $title = 'LessQ Operation-Local';
    }
    return $title;
  }

  


  /*************************** End Region Utils ***************************/





 	

  

  

  

  


	/*************************** End Region AJAX calls ***********************/




}