<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Sms extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->model('dailyq','',TRUE);
        $this->load->model('shift','',TRUE);
        $this->load->model('sms_log','',TRUE);
        $this->load->model('sms_code','',TRUE);

        // if(getenv('APPLICATION_ENV') == 'production')
        // {
        //     define('SENDER_URL_DIALOG', 'https://api.dialog.lk/sms/send');
        //     define('SENDER_URL_ETISALAT', 'http://dev.appzone.lk:7000/sms/send');
        // }
        // else if(getenv('APPLICATION_ENV') == 'staging')
        // {
        //     define('SENDER_URL_DIALOG', 'https://api.dialog.lk/sms/send');
        //     define('SENDER_URL_ETISALAT', 'http://dev.appzone.lk:7000/sms/send');
        // }
        // else 
        // {
        //     define('SENDER_URL_DIALOG', 'https://localhost:7443/sms/send');
        //     define('SENDER_URL_ETISALAT', 'https://localhost:7443/sms/send');
        // }
    }

    public function index()
    {
        echo $_SERVER['HTTP_HOST'];
    }

    public function dialog()
    {
        $inrec = file_get_contents('php://input');
        //insert log data
        $sms_log_id = $this->sms_log->insert_request($inrec, 'dialog');
        $this->sms_log->update_status($sms_log_id, 1);
        $array = json_decode($inrec, true);
        $sourceAddress = $array['sourceAddress'];
        $message = $array['message'];
        $requestId = $array['requestId'];
        $applicationId = $array['applicationId'];
        $encoding = $array['encoding'];
        $version = $array['version'];

        // $responses = array("statusCode" => "S1000", "statusDetail" => "Success");
        // header("Content-type: application/json");
        // echo json_encode($responses);

        //populate the response
        $responseMsg = $this->get_sms_reply($message, time(), $sms_log_id);
        //$responseMsg = "Dr. Nalaka in Dehiwala Medical Center - Seeing Patient No: 23";
        //$applicationId = "APP_009919";
        if($applicationId == "APP_011744") //staging
        {
            $password = "fb9c7451989f6357050f328e4d44bb59";
        }
        else if($applicationId == "APP_009919") //production
        {
            $password = "760daa5b4cdb455789162e8cdd1f162e";
        }

        $destinationAddresses = array($sourceAddress);
        $senderURL = SENDER_URL_DIALOG;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $applicationId,
            "password" => $password,
            "message" => $responseMsg,
            "destinationAddresses" => $destinationAddresses);
        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
        //insert log data
        $this->sms_log->insert_response($sms_log_id, $jsonObjectFields);

        $ch = curl_init($senderURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        //insert log data
        $this->sms_log->insert_result($sms_log_id, $res);

        // $myfile = fopen("/Users/chat/test/test3.txt", "w") or die("Unable to open file!");
        // $txt = date("h:i:sa")."----".$res."\n";
        // fwrite($myfile, $txt);
        // fclose($myfile);
    }

    public function etisalat()
    {
        $inrec = file_get_contents('php://input');
        //insert log data
        $sms_log_id = $this->sms_log->insert_request($inrec, 'etisalat');
        $this->sms_log->update_status($sms_log_id, 1);
        $array = json_decode($inrec, true);
        $sourceAddress = $array['sourceAddress'];
        $message = $array['message'];
        $requestId = $array['requestId'];
        $applicationId = $array['applicationId'];
        $encoding = $array['encoding'];
        $version = $array['version'];

        // $responses = array("statusCode" => "S1000", "statusDetail" => "Success");
        // header("Content-type: application/json");
        // echo json_encode($responses);

        //populate the response
        $responseMsg = $this->get_sms_reply($message, time(), $sms_log_id);
        //$responseMsg = "Dr. Nalaka in Dehiwala Medical Center - Seeing Patient No: 23";
        //$applicationId = "APP_009919";
        if($applicationId == "APP_001052") //stagind
        {
            $password = "73f64d9103e11feb5a931e03c138c52d";
        }
        else if($applicationId == "APP_001051") //live
        {
            $password = "f8c380dfa3f7fe770a2ccae5a0ddd821";
        }

        $destinationAddresses = array($sourceAddress);
        $senderURL = SENDER_URL_ETISALAT;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $applicationId,
            "password" => $password,
            "message" => $responseMsg,
            "destinationAddresses" => $destinationAddresses);
        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
        //insert log data
        $this->sms_log->insert_response($sms_log_id, $jsonObjectFields);

        $ch = curl_init($senderURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        //insert log data
        $this->sms_log->insert_result($sms_log_id, $res);

        // $myfile = fopen("/Users/chat/test/test3.txt", "w") or die("Unable to open file!");
        // $txt = date("h:i:sa")."----".$res."\n";
        // fwrite($myfile, $txt);
        // fclose($myfile);
    }

    public function sms_test()
    {
        $inrec = file_get_contents('php://input');
        $sms_data_id = $this->sms_log->insert_request($inrec);
        $array = json_decode($inrec, true);
        $sourceAddress = $array['sourceAddress'];
        $message = $array['message'];
        $requestId = $array['requestId'];
        $applicationId = $array['applicationId'];
        $encoding = $array['encoding'];
        $version = $array['version'];

        // $responseMsg = "Dr. Nalaka in Dehiwala Medical Center - Seeing Patient No: 23";
        $responseMsg = $this->get_sms_reply($message, time());
        echo $responseMsg;
       // echo time();
        // $applicationId = "APP_008305";
        // $password = "7248cffc44b9f71a0781d2b8217086ef";
        // $destinationAddresses = $sourceAddress;
        // $senderURL = 'https://localhost:7443/sms/send';

        // $arrayField = array("applicationId" => $applicationId,
        //     "password" => $password,
        //     "message" => $responseMsg,
        //     "destinationAddresses" => $destinationAddresses);
        // echo json_encode($arrayField);
    }

    // public function sms_test2($message, $time) {
    //     $crr_time  = explode("-",$time);
    //     $date = new DateTime('00:00:00');
    //     $date->setTime($crr_time[0], $crr_time[1]);
    //     $date = $date->format('Y/m/d H:i:s');
    //     $sms_log_id = $this->sms_log->insert_request($inrec, 'dialog');
    //     $this->sms_log->update_status($sms_log_id, 1);


    //     // $date = date('m/d/Y h:i:s a', time());
    //     // $date->setTime(12, 1);

    //     // echo $date;
    //     // $date->setTime(14, 55);
    //     $msg = 'lessq '.$message;
    //     // echo $this->get_sms_reply($msg, strtotime($date));
        
    // }

    private function get_sms_reply($smsmessage, $curr_time, $sms_log_id) 
    {
        $is_verified = false;
        $sms_code_data = null;
        $is_splitted_id = false;
        //sms format verification
        if(empty($smsmessage)) 
        {
            //not possble to hit on lq
            return "Empty request";
        }
        else
        {
            $smsmessage = trim($smsmessage);
            $smsparts = preg_split('/\s+/', $smsmessage);
            if(count($smsparts) < 2)
            {
                //not possble to hit on lq
                if($sms_log_id != -1)
                    $this->sms_log->update_status($sms_log_id, 0);
                //return "Invalid SMS format";
                  return "Customer Service person will reply you soon";
            }
            else
            {
                $msgPrefix = $smsparts[0]; //lessq
                $smsCode = trim($smsparts[1]);

                $res = $this->sms_code->get_hospital_doctor_sms_code($smsCode);
                if($res) 
                {
                    $is_verified = true;
                    $sms_code_data = $res;
                }
                else //is empty
                {
                    //check for third option
                    if(count($smsparts) > 2)
                    {
                        $smsCode = trim($smsparts[1]).trim($smsparts[2]);
                        $res = $this->sms_code->get_hospital_doctor_sms_code($smsCode);
                        if($res) 
                        {
                            $is_verified = true;
                            $sms_code_data = $res;
                            $is_splitted_id = true;
                        }
                        else
                        {
                            $smsCode = trim($smsparts[1]);
                            if($sms_log_id != -1)
                                $this->sms_log->update_status($sms_log_id, 0);
                            //return "Invalid LessQ ID \n \"$smsCode\".";
                            return "Customer Service person will reply you soon";
                        }
                    }
                    else
                    {
                        $smsCode = trim($smsparts[1]);
                        if($sms_log_id != -1)
                            $this->sms_log->update_status($sms_log_id, 0);
                        //return "Invalid LessQ ID \n \"$smsCode\".";
                        return "Customer Service person will reply you soon";
                    }
                }
            }
        }

        if($is_verified) //for safty
        {
            $sms_code_id = $sms_code_data->id;
            $doctor_name = $sms_code_data->doctor_name;
            $hospital_name = $sms_code_data->hospital_name;
            //check the code in dailyq
            $res = $this->dailyq->get_queues_for_smscode($sms_code_id);

            if(!$res) //is empty
            {
                return "$doctor_name is not available at $hospital_name on today";
            }
            else
            {
                $selectedQ = null;
                $selectedQPre = null;
                $selectedSlot = -1;
                if($curr_time <= strtotime($res[0]->end_time))
                {
                    $selectedSlot = 0;
                    $selectedQ = $res[0];
                } 
                else
                {
                    
                    for ($i = 1; $i < count($res); $i++) 
                    {
                        $cq_start_time = strtotime($res[$i]->start_time);
                        $cq_end_time = strtotime($res[$i]->end_time);

                        $pq_start_time = strtotime($res[$i - 1]->start_time);
                        $pq_end_time = strtotime($res[$i - 1]->end_time);
                        if ($curr_time > $pq_end_time && $curr_time <= $cq_end_time) 
                        {
                            $selectedSlot = $i;
                        }
                    }

                    if($selectedSlot == -1)
                    {
                        $selectedSlot = count($res) - 1;
                    }
                    // return "ID $selectedSlot";
                }
                if($selectedSlot == -1)
                {
                    return "Sorry. The request cannot be processed.";
                }
                else if($selectedSlot == 0)
                {
                    $selectedQ = $res[0];
                }
                else
                {
                    $previousQ = $res[$selectedSlot - 1];
                    if($previousQ->status == 1 && $res[$selectedSlot]->status != 1)
                    {
                        $selectedQ = $previousQ;
                    }
                    else
                    {
                        $selectedQ = $res[$selectedSlot];
                    }
                }

                //process the message
                $st_time = date("h:i a", strtotime($selectedQ->start_time));
                $en_time = date("h:i a", strtotime($selectedQ->end_time));
                $retMsg = "$doctor_name \n";
                if($selectedQ->status == 0)//not started
                {
                    $retMsg = $retMsg."has not yet arrived";
                }
                else if($selectedQ->status == 1)//started
                {
                    if($selectedQ->patient_count == 0)
                    {
                        $started_time = date("h:i a", strtotime($selectedQ->started));
                        $retMsg = $retMsg."arrived at $started_time";
                    }
                    else
                    {
                        $retMsg = $retMsg."seeing patient No.$selectedQ->patient_count";
                    }
                }
                else if($selectedQ->status == 2)//finished
                {
                    //$updated = date("H:i",strtotime($selectedQ->updated));
                    $updated = date("h:i a", strtotime($selectedQ->updated));
                    $retMsg = $retMsg."finished at $updated";
                }
                else if($selectedQ->status == 3)//eat
                {
                    $retMsg = $retMsg."arrival expecting at $selectedQ->eat";
                }
                $retMsg = $retMsg.", \nChanneling hours: $st_time - $en_time, \n";
                $retMsg = $retMsg."$hospital_name";
                if($is_splitted_id)
                {
                    $retMsg = $retMsg."\nProper SMS format: lessq<space>$smsCode";
                }
                return $retMsg;
            }
            // echo '</br>selectedSlot '.$selectedSlot;
        }
    }
}





 
/* End of file Blog.php */
/* Location: ./application/controllers/blog.php */