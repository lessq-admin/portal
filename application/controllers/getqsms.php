<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Getqsms extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->model('dailyb','',TRUE);
        $this->load->model('dailyb_log','',TRUE);
        $this->load->model('dailyq','',TRUE);
        $this->load->model('shift','',TRUE);
        $this->load->model('sms_log','',TRUE);
        $this->load->model('sms_code','',TRUE);
    }

    public function index()
    {
        echo $_SERVER['HTTP_HOST'];
    }

    public function dialog()
    {
        $inrec = file_get_contents('php://input');
        //insert log data
        $sms_log_id = $this->sms_log->insert_request($inrec, 'dialog');
        $this->sms_log->update_status($sms_log_id, 3);
        $array = json_decode($inrec, true);
        $sourceAddress = $array['sourceAddress'];
        $message = $array['message'];
        $requestId = $array['requestId'];
        $applicationId = $array['applicationId'];
        $encoding = $array['encoding'];
        $version = $array['version'];

        //populate the response
        $responseMsg = $this->get_sms_reply($message, time(), $sms_log_id);
        if($applicationId == "APP_016724") //staging
        {
            $password = "245c7ac3825e49bf65bf0f9e3856231d";
        }
        else if($applicationId == "APP_016723") //production
        {
            $password = "b31ea0073b0529b81655cb6bc10b6177";
        }

        $destinationAddresses = array($sourceAddress);
        $senderURL = SENDER_URL_DIALOG;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $applicationId,
            "password" => $password,
            "message" => $responseMsg,
            "destinationAddresses" => $destinationAddresses);
        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
        //insert log data
        $this->sms_log->insert_response($sms_log_id, $jsonObjectFields);

        $ch = curl_init($senderURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        //insert log data
        $this->sms_log->insert_result($sms_log_id, $res);
    }

    public function etisalat()
    {
        $inrec = file_get_contents('php://input');
        //insert log data
        $sms_log_id = $this->sms_log->insert_request($inrec, 'etisalat');
        $this->sms_log->update_status($sms_log_id, 3);
        $array = json_decode($inrec, true);
        $sourceAddress = $array['sourceAddress'];
        $message = $array['message'];
        $requestId = $array['requestId'];
        $applicationId = $array['applicationId'];
        $encoding = $array['encoding'];
        $version = $array['version'];

        //populate the response
        $responseMsg = $this->get_sms_reply($message, time(), $sms_log_id);
        if($applicationId == "APP_001463") //stagind
        {
            $password = "b3075b9df843809ea7be60a58c622d37";
        }
        else if($applicationId == "APP_001462") //production
        {
            $password = "75af9ac05a7efa72217eb1867ee8deb1";
        }

        $destinationAddresses = array($sourceAddress);
        $senderURL = SENDER_URL_ETISALAT;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $applicationId,
            "password" => $password,
            "message" => $responseMsg,
            "destinationAddresses" => $destinationAddresses);
        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
        //insert log data
        $this->sms_log->insert_response($sms_log_id, $jsonObjectFields);

        $ch = curl_init($senderURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        //insert log data
        $this->sms_log->insert_result($sms_log_id, $res);
    }

    // public function sms_test()
    // {
    //     $inrec = file_get_contents('php://input');
    //     $sms_data_id = $this->sms_log->insert_request($inrec);
    //     $array = json_decode($inrec, true);
    //     $sourceAddress = $array['sourceAddress'];
    //     $message = $array['message'];
    //     $requestId = $array['requestId'];
    //     $applicationId = $array['applicationId'];
    //     $encoding = $array['encoding'];
    //     $version = $array['version'];

    //     // $responseMsg = "Dr. Nalaka in Dehiwala Medical Center - Seeing Patient No: 23";
    //     $responseMsg = $this->get_sms_reply($message, time());
    //     echo $responseMsg;
    //    // echo time();
    //     // $applicationId = "APP_008305";
    //     // $password = "7248cffc44b9f71a0781d2b8217086ef";
    //     // $destinationAddresses = $sourceAddress;
    //     // $senderURL = 'https://localhost:7443/sms/send';

    //     // $arrayField = array("applicationId" => $applicationId,
    //     //     "password" => $password,
    //     //     "message" => $responseMsg,
    //     //     "destinationAddresses" => $destinationAddresses);
    //     // echo json_encode($arrayField);
    // }

    public function sms_test($message) {
//        $sms_log_id = $this->sms_log->insert_request("test sms", 'dialog');
                

        // $date = date('m/d/Y h:i:s a', time());
        // $date->setTime(12, 1);

        // echo $date;
        // $date->setTime(14, 55);
        $msg = 'lessq '.$message;
        echo $this->get_sms_reply($msg, time(), -1);
    }

    private function get_sms_reply($smsmessage, $curr_time, $sms_log_id) 
    {
        $is_verified = false;
        $sms_code_data = null;
        $is_splitted_id = false;
        //sms format verification
        if(empty($smsmessage)) 
        {
            //not possble to hit on lq
            return "Empty request";
        }
        else
        {
            $smsmessage = trim($smsmessage);
            $smsparts = preg_split('/\s+/', $smsmessage);
            if(count($smsparts) < 2)
            {
                //not possble to hit on lq
                return "Invalid SMS format";
            }
            else
            {
                $msgPrefix = $smsparts[0]; //getq
                $smsCode = trim($smsparts[1]);

                $res = $this->sms_code->get_hospital_doctor_sms_code($smsCode);
                if($res) 
                {
                    $is_verified = true;
                    $sms_code_data = $res;
                }
                else //is empty
                {
                    //check for third option
                    if(count($smsparts) > 2)
                    {
                        $smsCode = trim($smsparts[1]).trim($smsparts[2]);
                        $res = $this->sms_code->get_hospital_doctor_sms_code($smsCode);
                        if($res) 
                        {
                            $is_verified = true;
                            $sms_code_data = $res;
                            $is_splitted_id = true;
                        }
                        else
                        {
                            $smsCode = trim($smsparts[1]);
                            if($sms_log_id != -1)
                                $this->sms_log->update_status($sms_log_id, 2);
                            return "Invalid LessQ ID \n \"$smsCode\".";
                        }
                    }
                    else
                    {
                        $smsCode = trim($smsparts[1]);
                        if($sms_log_id != -1)
                            $this->sms_log->update_status($sms_log_id, 2);
                        return "Invalid LessQ ID \n \"$smsCode\".";
                    }
                }
            }
        }

        if($is_verified) //for safty
        {
            $sms_code_id = $sms_code_data->id;
            $doctor_name = $sms_code_data->doctor_name;
            $hospital_name = $sms_code_data->hospital_name;
            //check the code in dailyq
            $res = $this->dailyq->get_queues_for_smscode($sms_code_id);

            if(!$res) //is empty
            {
                return "$doctor_name is not available at $hospital_name on today";
            }
            else
            {
                $start_time = "7:00 am";
                $close_time = "1:00 pm";
                $current_time = date("h:i a", strtotime('now'));
                $date1 = DateTime::createFromFormat('H:i a', $current_time);
                $date2 = DateTime::createFromFormat('H:i a', $start_time);
                $date3 = DateTime::createFromFormat('H:i a', $close_time);
                //echo date("h:i a", strtotime($date1));
                if ($date1 < $date2 || $date1 > $date3)
                {
                   return "Booking by SMS at Dr.M Healthcare is\n from 7:00 am to 1:00 pm ONLY.\nPlease visit to get a number after 1:00 pm.\nLessQ Customer Support: 076 920 7669";
                }

                //assume same doctor does not have many queues at same day

                $selectedQ = $res[0];
                //if finished no booking possible
                if($selectedQ->status == 2)//finished
                {
                    $updated = date("h:i a", strtotime($selectedQ->updated));
                    $retMsg = $retMsg."finished at $updated";
                }
                else if($selectedQ->status == 4)//canceled
                {
                    $updated = date("h:i a", strtotime($selectedQ->updated));
                    $retMsg = $retMsg."finished at $updated";
                }
                else //status for not arrived, operating, eat
                {
                    $selectedB = $this->dailyb->get_dailyb_by_dailyq_id($selectedQ->id);
                    if(!$selectedB)
                    {
                        return "$doctor_name has not activated SMS booking at $hospital_name";
                    }
                    else
                    {
                        //update booking tabel
                        $appointment_number = $this->dailyb->add_booking($selectedB->id);//sms booking
                        $reference = $this->add_booking_reference($selectedB->id, $appointment_number, 1);
                        $st_time = date("h:i a", strtotime($selectedQ->start_time));
                        $en_time = date("h:i a", strtotime($selectedQ->end_time));
                        //TODO:add data to booking table
                        if($appointment_number > 0)
                        {
                            // $retMsg = "Booking confirmed at ".get_current_time()."\n";
                            $retMsg = "";
                            $retMsg = $retMsg."Appointment No: $appointment_number \n";
                            $display_date = date("M d", strtotime(get_current_date()));
                            $dispaly_reference = str_pad($reference, 5, '0', STR_PAD_LEFT); 
                            $retMsg = $retMsg."$display_date \n";
                            $retMsg = $retMsg."$doctor_name \n";

                            //geting q status
                            if($selectedQ->status == 1)//started
                            {
                                $waiting = $selectedB->nappo - $selectedQ->patient_count;
                                 $retMsg = $retMsg."seeing patient No.$selectedQ->patient_count";
                            }
                            else if($selectedQ->status == 3)//eat
                            {
                                $qStatusMsg = "Doctor expecting at $selectedQ->eat";
                            }
                            
                            $retMsg = $retMsg.",\nStart time: $st_time\n";
                            $retMsg = $retMsg."$hospital_name\n";
                            $retMsg = $retMsg."Ref: $dispaly_reference\n";
                            $retMsg = $retMsg."Check the queue, \n lessq $smsCode\n";
                        }
                        else
                        {
                            return "Sorry. The request cannot be processed.\n Sorry for the inconvenience";
                        }
                    }                    
                }
                if($is_splitted_id)
                {
                    $retMsg = $retMsg."\nProper SMS format: lessq<space>$smsCode";
                }
                return $retMsg;
            }
        }
    }

    private function add_booking_reference($dailyb_id, $nappo, $type)
    {
        $dailyq = $this->dailyb->get_dailyq_by_dailyb_id($dailyb_id);
        $shift_type = $dailyq->shift_type;
        $shift_special_id = $dailyq->shift_special_id;
        $shift_regular_id = $dailyq->shift_regular_id;
        $reference = $this->dailyb_log->insert($shift_type, $shift_special_id, $shift_regular_id, $nappo, $type);
        return $reference;
    }
}





 
/* End of file Blog.php */
/* Location: ./application/controllers/blog.php */