<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lqserviceportal extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}

   		$this->load->model('dailyq','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('shift','',TRUE);
   		$this->load->model('sms_log','',TRUE);
   		$this->load->model('dailyq_log','',TRUE);
   		$this->load->model('doctor','',TRUE);
   		$this->load->model('sms_code','',TRUE);
   		$this->load->model('device','', TRUE);
   		$this->load->model('dailyb','', TRUE);
   		$this->load->model('dailyb_log','', TRUE);
 	}

 	public function test()
 	{
 		$this->load->library('utils');
 		print_r($this->utils->test());
 	}

 	public function index()
	{
		// $user = $this->ion_auth->user()->row();
		// echo $user->first_name.' '.$user->last_name;
		// if (!$this->ion_auth->logged_in())
		// {
		// 	// redirect them to the login page
		// 	redirect('auth/login', 'refresh');
		// }

		if(getenv('APPLICATION_ENV') == 'production')
        {
            $title = 'LessQ Service Portal';
        }
        else if(getenv('APPLICATION_ENV') == 'staging')
        {
            $title = 'LessQ Service Portal-Staging';
        }
        else 
        {
            $title = 'LessQ Service Portal-Local';
        }
        $data['title'] = $title;
		$this->load->view('sp', $data);
	}

	private function update_table()
	{
		$this->dailyb->drop_table();
		$this->dailyq->drop_table();

		$this->dailyq->create_table();
		$this->dailyb->create_table();

		$this->dailyq->fill_table();
		$this->dailyb->fill_table();
	}

	public function reset_dailyq()
	{
		$txt_password = $this->input->post('txt_password');
		if($txt_password == null)
		{
			$this->load->view('reset_dailyq', '');
		}else {
			if($txt_password == "lessq" ) {
				$this->update_table();
				$data = array('msg' => 'Reset dailyQ successfilly');
				$this->load->view('reset_dailyq', $data);
			}
			else
			{
				$data = array('msg' => 'Invalid password');
				$this->load->view('reset_dailyq', $data);
			}
		}
	}

// This method is only for cron jobs
	public function cron_reset_dailyq()
	{
		$whitelist = array(
		    '127.0.0.1',
		    '::1'
		);
		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist) and $this->input->post('password') == "M@t5umut0")
		{
			$this->update_table();
			echo 'Operation completed';
		}
		else 
		{
			echo 'Operation failed';
		}
	}

	public function display_shifts()
	{
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--Select--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
        $this->load->view('display_shifts', $data);
	}

	public function show_sms_log()
	{
		$data["logs"] = $this->sms_log->get_sms_log();
		$this->load->view('show_sms_log', $data);
	}

	public function sms_filter()
	{
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--Select--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
		$min_date = $this->sms_log->get_min_date();
		if(!$min_date)
			$data['min_date'] = date('Y-m-d', time());
		else
			$data['min_date'] = $min_date;
		$data['max_date'] = date('Y-m-d', time());
        $this->load->view('sms_filter', $data);
	}

	public function sms_filter_result()
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$from_date = $this->input->post('from_date');
		$to_date = $this->input->post('to_date');
		$res = $this->sms_log->get_sms_for_dates($from_date, $to_date);
		
		$add_arr = array();
		$data_arr = array();
		foreach ($res as $key => $val)
		{
			echo $val->id.'</br>';
			$request = $val->request;
			$obj = json_decode($request);
			$add_arr[] = $obj->sourceAddress;
			$row['id'] = $val->id;
			$row['carrier'] = $val->carrier;
			$row['tel'] = $obj->sourceAddress;

			array_push($data_arr, $row);
		}
		$add_arr_cnt = array_count_values($add_arr);

		//iterating and making display arr
		$display_arr = array();
		$cnt = 1;
		$tot_sms = 0;
		foreach ($add_arr_cnt as $key => $val)
		{
			$carrier = '';
			foreach ($data_arr as $key1 => $val1)
			{
				if ($val1['tel'] === $key) {
					$carrier = $val1['carrier'];
		        }
			}
			$drow = array(
			'no' => $cnt++,
			'tel' => $key,
			'cnt' => $val,
			'carrier' => $carrier);
			$tot_sms += intval($val);
			// array_push($display_arr, $drow);
			$display_arr[] = (object)$drow;
			// print_r($drow);
		}
		$data["tbl_data"] = $display_arr;
		$data["from_date"] = $from_date;
		$data["to_date"] = $to_date;
		$data["tot_sms"] = $tot_sms;
		$data["nusers"] = count($add_arr_cnt);
		$this->load->view('sms_filter_results', $data);

	}

	function q_operation_logs(){
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--Select--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
		$currdate = date('Y-m-d', time());
		$pre_date = date('Y-m-d', strtotime($currdate .' -1 day'));
		$data['pre_date'] = $pre_date;
        $this->load->view('q_operation_logs', $data);
	}

	

	function cancel_shift_confirm()
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$date = $this->input->post('txt_date');
		$curr_date = date('Y-m-d', time());
		$check_list = $this->input->post('check_list'); 
		// echo $date;
		if(!empty($check_list)) 
		{
			foreach($check_list as $check) 
			{
				$check_id_parts = explode("_", $check);
				$type = $check_id_parts[0];
				$shift_id = $check_id_parts[1];
				
				if($type == 'reg')
				{
					$this->shift->cancel_regular_shift($shift_id, $date);
					if($curr_date == $date)
					{
						$this->dailyq->cancel_regular_shift($shift_id, $date);
					}
				}
				else if($type == 'spe')
				{
					$this->shift->cancel_special_shift($shift_id, $date);
					if($curr_date == $date)
					{
						$this->dailyq->cancel_special_shift($shift_id, $date);
					}
				}
			}
		}

        $this->session->set_flashdata('msg', 'Shift cancelation completed');

  		redirect(site_url('internal/cancel_shift'), 'refresh'); 
	}

	//this function return JSON of all shifts in selected 
	//hospital
	function get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date){
		$dateObj = DateTime::createFromFormat('Y-m-d', $date);
		$dayofweek = $dateObj->format('w');

		$data_array = $this->shift->get_regular_shift_for_hospital_doctor_day_of_week($hospital_id, $doctor_id, $dayofweek);
		//check for cancelations
		// if($data_array) 
		// {
			foreach ($data_array as $row)
	        {
	          	$shift_regular_id = $row->shift_id;
				if($this->shift->get_cancel_shifts($shift_regular_id, $date))
				{
					$row->cancelled = 1;
				}
				else
				{
					$row->cancelled = 0;
				}
				$row->type = 'Regular';
			}
		// }
		//get special shifts
		$special_shifts = $this->shift->get_special_shift_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
		// if($special_shifts) 
		// {
			foreach ($special_shifts as $row)
	        {
	        	if($row->status == 2)
				{
					$row->cancelled = 1;
				}
				else
				{
					$row->cancelled = 0;
				}
				$row->type = 'Special';
				$data_array[] = $row;
			}
		// }
		return $data_array;
	}
	function json_get_shifts_for_hospital_doctor_date() 
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$doctor_id = $this->input->post('ddl_doctors');
		$date = $this->input->post('txt_date');
		$data_array = $this->get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
		echo json_encode($data_array);
		// print_r($data_array);
		// echo $hospital_id;
	}

	function json_get_regular_shifts_for_hospital() 
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$data_array = $this->shift->get_regular_shift_for_hospital_doctor_day_of_week($hospital_id, 0, -1);
		echo json_encode($data_array);
		// print_r($data_array);
		// echo $hospital_id;
	}

	function json_q_operation_log()
	{
		// $date = '2015-07-02';
		// $hospital_id = 2;
		$log_data = array();
		$hospital_id = $this->input->post('hospital_id');
		$date = $this->input->post('date');

		$shift_regular_ids = $this->dailyq_log->get_regular_shift_ids_for_hospital_date($hospital_id, $date);

		$shift_special_ids = $this->dailyq_log->get_special_shift_ids_for_hospital_date($hospital_id, $date);

		if($shift_regular_ids || $shift_special_ids)
		{
			if($shift_regular_ids)
			{
				foreach ($shift_regular_ids as $key => $val)
				{
					$record = array();

					$shift = $this->shift->get_regular_shift($val->shift_regular_id);
					$record["shift_type"] = 'Regular';
					$record["doctor_name"] = $shift->display_name;
					$record["date"] = $date;
					$record["start_time"] = $shift->start_time;
					$record["end_time"] = $shift->end_time;

					$patient_cnt_logs = $this->dailyq_log->get_shift_regular_logs($shift->shift_id, $date);

					$table = array();
					$table['cols'] = array(
						array('label' => 'time', 'type' => 'string'),
				    	array('label' => 'Patients', 'type' => 'number'),
				    	array('type' => 'string', 'role' => 'annotation')
					);

					$rows = array();
					foreach ($patient_cnt_logs as $key => $val) 
					{
						$temp = array();
			      		$temp[] = array('v' => $val->time_stamp); 
			      		$temp[] = array('v' => $val->patient_count); 
			      		if($val->msg_status == 1)
			      			$temp[] = array('v' => 'Start');
			      		else if($val->msg_status == 3)
			      			$temp[] = array('v' => 'Finish'); 
			      		else if($val->msg_status == 4)
			      			$temp[] = array('v' => 'Reset');
			      		else if($val->msg_status == 100)
			      			$temp[] = array('v' => 'Start Error');
			      		else if($val->msg_status == 200)
			      			$temp[] = array('v' => 'Update Error');
			      		else if($val->msg_status == 300)
			      			$temp[] = array('v' => 'Finish Error');
			      		else if($val->msg_status == 400)
			      			$temp[] = array('v' => 'Reset Error');
			      		
			      		$rows[] = array('c' => $temp);
			    	}
			    	$table['rows'] = $rows;
			    	$record["table"] = $table;
			    	$log_data[] = $record;
				}
			}
			//special shifts
			if($shift_special_ids)
			{
				foreach ($shift_special_ids as $key => $val)
				{
					$record = array();

					$shift = $this->shift->get_special_shift($val->shift_special_id);
					$record["shift_type"] = 'Special';
					$record["doctor_name"] = $shift->display_name;
					$record["date"] = $date;
					$record["start_time"] = $shift->start_time;
					$record["end_time"] = $shift->end_time;

					$patient_cnt_logs = $this->dailyq_log->get_shift_special_logs($shift->shift_id, $date);

					$table = array();
					$table['cols'] = array(
						array('label' => 'time', 'type' => 'string'),
				    	array('label' => 'Patients', 'type' => 'number'),
				    	array('type' => 'string', 'role' => 'annotation')
					);

					$rows = array();
					foreach ($patient_cnt_logs as $key => $val) 
					{
						$temp = array();
			      		$temp[] = array('v' => $val->time_stamp); 
			      		$temp[] = array('v' => $val->patient_count); 
			      		if($val->msg_status == 1)
			      			$temp[] = array('v' => 'Start');
			      		else if($val->msg_status == 3)
			      			$temp[] = array('v' => 'Finish'); 
			      		else if($val->msg_status == 4)
			      			$temp[] = array('v' => 'Reset');
			      		else if($val->msg_status == 100)
			      			$temp[] = array('v' => 'Start Error');
			      		else if($val->msg_status == 200)
			      			$temp[] = array('v' => 'Update Error');
			      		else if($val->msg_status == 300)
			      			$temp[] = array('v' => 'Finish Error');
			      		else if($val->msg_status == 400)
			      			$temp[] = array('v' => 'Reset Error');
			      		$rows[] = array('c' => $temp);
			    	}
			    	$table['rows'] = $rows;
			    	$record["table"] = $table;
			    	$log_data[] = $record;
				}
			}
		}
		else
		{
			$log_data["error"] = "No any queues operated on the selected date";
		}

		// print_r($log_data);
		$json_data= json_encode($log_data);

		echo $json_data;

		return;

		///////


		$shift_ids = $this->dailyq_log->get_distinct_shift_ids_for($hospital_id, $date);
		// print_r($shift_ids);
// return;
		
		if($shift_ids)
		{
			foreach ($shift_ids as $key => $val)
			{
				$record = array();

				$shift = $this->shift->get_shift($val->shift_id);
				$record["shift_id"] = $shift->shift_id;
				$record["doctor_name"] = $shift->display_name;
				$record["date"] = $date;
				$record["start_time"] = $shift->start_time;
				$record["end_time"] = $shift->end_time;

				$patient_cnt_logs = $this->dailyq_log->get_logs($shift->shift_id, $date);

				$table = array();
				$table['cols'] = array(
					array('label' => 'time', 'type' => 'string'),
			    	array('label' => 'Patients', 'type' => 'number')
				);

				$rows = array();
				foreach ($patient_cnt_logs as $key => $val) 
				{
					$temp = array();
		      		$temp[] = array('v' => $val->time_stamp); 
		      		$temp[] = array('v' => $val->patient_count); 
		      		$rows[] = array('c' => $temp);
		    	}
		    	$table['rows'] = $rows;
		    	$record["table"] = $table;
		    	$log_data[] = $record;
			}
		}
		else
		{
			$log_data["error"] = "No any queues operated on the selected date";
		}

		// print_r($log_data);
		$json_data= json_encode($log_data);

		echo $json_data;
	}

	function add_temporary_shift()
	{
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--Select--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
		//make dummay doctor list
		$doctor_list = array();
      	$doctor_list['0'] = '--All--'; 
		$data['doctor_list'] = $doctor_list;
		$data['date'] = date('Y-m-d', time());
		$this->load->view('add_temporary_shift', $data);
	}

	function add_temporary_shift_confirm()
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$doctor_id = $this->input->post('ddl_doctors');
		$date = $this->input->post('txt_date');
		$start_time = $this->input->post('ddl_start_time');
		$end_time = $this->input->post('ddl_end_time');
		$curr_date = date('Y-m-d', time());

		$msg = '';
		$validate = true;

		//validation of inputs
		if($hospital_id <= 0)
		{
			$msg = 'Invalid hospital';
			$validate = false;
		}
		else if($doctor_id <= 0)
		{
			$msg = 'Invalid doctor';
			$validate = false;
		}
		else if(!validate_date($date, 'Y-m-d'))//check valid date)
		{
			$msg = 'Invalid date';
			$validate = false;
		}
		else
		{
			//check time range
			$start_time = date('H:i', strtotime($start_time));
			$end_time = date('H:i', strtotime($end_time));
			// echo $start_time;return;

			if($end_time <= $start_time)
			{
				$msg = 'Invalid start and end time';
				$validate = false;
			}
			else
			{
				$shift_array = $this->get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
				// print_r($shift_array);
				$is_overlapped = false;
				foreach($shift_array as $row) {
					// echo $row ->cancelled.' '.$start_time.' '.$end_time.'¥n';
					if($row ->cancelled == '0' && (($start_time >= $row ->start_time) && ($start_time <= $row ->end_time) || 
					($end_time >= $row ->start_time) && ($end_time <= $row ->end_time)))
					{
						$is_overlapped = true;
						break;
					} 
				}
			
				//verifies overlaps of staart and end time
				if($is_overlapped)
				{
					$msg = 'Start and end times are overlapped with the existing shift';
					$validate = false;
				}
			}
		}

		if(!$validate)
		{
			//return erroe to the page
			echo $msg.' populating selected data is under construction';
		}
		else
		{
			$start_time = date('0000-00-00 H:i:s', strtotime($start_time));
      		$end_time = date('0000-00-00 H:i:s', strtotime($end_time));
			$sms_code_id = $this->sms_code->get_sms_code($hospital_id, $doctor_id)->id;
			$shift_special_id = $this->shift->add_special_shift($date, $start_time, $end_time, $sms_code_id);
			if($curr_date == $date)
			{
				$this->dailyq->add_special_shift($shift_special_id, $date, $start_time, $end_time, $sms_code_id);
			}
			$this->session->set_flashdata('msg', 'Shift insertion completed');

  			redirect(site_url('internal/add_temporary_shift'), 'refresh'); 
		}
	}

	public function gen_device_uid()
    {
    	$guid = '';
    	do 
    	{
	    	$guid = sprintf( '%04x-%04x-%04x-%04x',
		        mt_rand( 0, 0xffff ), 
		        mt_rand( 0, 0xffff ),
		        mt_rand( 0, 0x0fff ) | 0x4000,
		        mt_rand( 0, 0x3fff ) | 0x8000,
		        mt_rand( 0, 0xffff ));
		}
		while($this->device->check_uid($guid));
	    //check the values with db
	    echo($guid);
    }

    public function show_devices()
    {
    	$data['devices'] = $this->device->get_devices();
    	$this->load->view('display_devices', $data);
    }

    /************************************************************************/
	/*************************** Region UI calls ****************************/
	/************************************************************************/

	function cancel_shift()
	{
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--Select--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
		$data['date'] = date('Y-m-d', time());
		$doctor_list = array();
      	$doctor_list[''] = '--All--'; 
		$data['doctor_list'] = $doctor_list;
        $this->load->view('cancel_shift', $data);
	}

	public function sms_reply()
	{
		$hoslpital_obj_list = $this->hospital->get_all_hospitals();
		$hoslpital_list['0'] = '--All--'; 
		if($hoslpital_obj_list) {
			foreach($hoslpital_obj_list as $row) {
				$hoslpital_list[$row->id] = $row->name;
			}
		}
		$data['hospital_list'] = $hoslpital_list;
		$data['from_date'] = date('Y-m-d', strtotime("-7 days"));
		$data['to_date'] = date('Y-m-d', time());
		$doctor_list = array();
      	$doctor_list[''] = '--All--'; 
		$data['doctor_list'] = $doctor_list;
		$sms_status_list['-1'] = '--All--'; 
		$sms_status_list['1'] = 'Success'; 
		$sms_status_list['0'] = 'Failed'; 
		$data['sms_status'] = $sms_status_list;

        $this->load->view('sms_reply', $data);
	}

	public function sms_reply_search_results()
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$doctor_id = $this->input->post('ddl_doctors');
		$from_date = $this->input->post('from_date');
		$to_date = $this->input->post('to_date');
		$sms_status = $this->input->post('ddl_sms_status');

		$sms_codes = array();
		if($hospital_id != 0 && $doctor_id != 0)
		{
			$sms_code = $this->sms_code->get_sms_code($hospital_id, $doctor_id);
			$sms_codes[] = (object)$sms_code;
		}
		else if($hospital_id != 0) //doctor_id == 0
		{
			$res = $this->sms_code->get_sms_codes_for_hospital($hospital_id);
			foreach($res as $key => $sms_code)
			{
				$sms_codes[] = (object)$sms_code;
			}
		}

		$tbl_data = array();
		$res_array = array();
		if(count($sms_codes) > 0)
		{
			foreach ($sms_codes as $key => $row)
			{ 
				$sms_code = $row->code;
				$res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
				foreach ($res as $key => $row)
				{
					$res_array[] = (object)$row;
				}
			}
		}
		else
		{
			$sms_code = '';
			$res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
			foreach ($res as $key => $row)
			{
				$res_array[] = (object)$row;
			}
		}

		$cnt = 0;
		foreach ($res_array as $key => $val)
		{
			$request = $val->request;
			$response = $val->response;
			$obj_req = json_decode($request);
			$obj_res = json_decode($response);
			if(strlen($obj_req->message) > 12)
				$req_msg = substr($obj_req->message, 0, 20).'...'; //
			else
				$req_msg = $obj_req->message;
			$res_msg = substr($obj_res->message, 0, 50).'...'; 
			$tel     = '...'.substr($obj_req->sourceAddress, -5); 
			$date_time = $val->request_time;
			$carrier = $val->carrier;
			$id = $val->id;
			$row = array(
				'no' => $cnt++,
				'log_id' => $id,
				'carrier' => $carrier,
				'date_time' => $date_time,
				'tel' => $tel,
				'req_msg' => $req_msg,
				'res_msg' => $res_msg
			);
			$tbl_data[] = (object)$row;
		}

		//preparing display data
		if($hospital_id == 0)
			$selected_hospital = '--All--';
		else
			$selected_hospital = $this->hospital->get_hospital($hospital_id)->hospital_name;
		if($doctor_id == 0)
			$selected_doctor = '--All--';
		else
			$selected_doctor = $this->doctor->get_doctor($doctor_id)->display_name;
		$sms_status_list['-1'] = '--All--'; 
		$sms_status_list['1'] = 'Success'; 
		$sms_status_list['0'] = 'Failed'; 
		// echo $sms_status; return;
		$selected_sms_status = $sms_status_list[$sms_status];

		$data['selected_hospital'] = $selected_hospital;
		$data['selected_doctor'] = $selected_doctor;
		$data['selected_sms_status'] = $selected_sms_status;
		$data['selected_from_date'] = $from_date;
		$data['selected_to_date'] = $to_date;
		$data['tbl_data'] = $tbl_data;

		$this->load->view('sms_reply_results', $data);
	}

	function sms_reply_confirm()
	{
		$sms_text = $this->input->post('txt_sms');
		$check_list = $this->input->post('check_list'); 
		// echo count($check_list);
		// echo $date;
		if(!empty($check_list)) 
		{
			foreach($check_list as $log_id) 
			{
				// echo $log_id;
				$sms_log = $this->sms_log->get_sms_log_for_id($log_id);
				$carrier = $sms_log->carrier;
				$response = $sms_log->response;
				$obj_res = json_decode($response);
				if($carrier == 'dialog')
					$senderURL = SENDER_URL_DIALOG;//'https://localhost:7443/sms/send';
				if($carrier == 'etisalat')
					$senderURL = SENDER_URL_ETISALAT;//'https://localhost:7443/sms/send';

				$arrayField = array("applicationId" => $obj_res->applicationId,
		            "password" => $obj_res->password,
		            "message" => $sms_text,
		            "destinationAddresses" => $obj_res->destinationAddresses);
		        $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
		        //TODO:make log in database

		        $ch = curl_init($senderURL);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POST, 1);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        $res = curl_exec($ch);
		        curl_close($ch);
			}
			$this->session->set_flashdata('msg', 'SMS reply completed');
		}
		else
		{
			$this->session->set_flashdata('msg', 'Error occured </br> Start from begining');
		}
		redirect(site_url('internal/sms_reply'), 'refresh'); 
	}

	function show_bookings()
	{
		$data['bookings'] = $this->dailyb->get_bookings();
		$data['date'] = get_current_date();
    	$this->load->view('display_bookings', $data);
	}

	/*************************** END Region UI calls ************************/

	/************************************************************************/
	/************************* Region AJAX calls ****************************/
	/************************************************************************/

	function get_doctors_for_hospital()
	{
		$hospital_id = $this->input->post('ddl_hospitals');
		$data_array = $this->doctor->get_doctors_for_hospital($hospital_id);
		$zero_index = array('doctor_id'=>'0', 'display_name' => '--All--');
		array_unshift($data_array , $zero_index);
		echo json_encode($data_array);
	}

	

	/*************************** End Region AJAX calls ***********************/




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */