<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bservice extends CI_Controller {

	function __construct()
 	{
   		parent::__construct();
   		$this->load->model('device','',TRUE);
   		$this->load->model('shift','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('dailyq','',TRUE);
   		$this->load->model('dailyq_log','',TRUE);
   		$this->load->model('constants','',TRUE);
   		$this->load->model('dailyb','',TRUE);
   		$this->load->model('dailyb_log','',TRUE);
 	}

	public function index()
	{
		echo "Access is forbidden." ;
	}


/************************************************************************/
/********************** Region BookingAPP functions **********************/
/************************************************************************/

	public function get_init_data()
	{
		$response = null;
		$passcode = $this->input->post('passcode');
		$uid = $this->input->post('uid');
		// $passcode = '0011';
		// $uid = 'fa32-4720-4f82-a41f';
		// echo $this->device->check_uid($uid)->id;
		if(!empty($uid) && !empty($passcode) && $this->device->check_uid($uid) !== false) 
		{
			$device = $this->verify_device();
			if($device) 
			{
				//echo $result->hospital_id;
				$hospital = $this->hospital->get_hospital($device->hospital_id);
				$data_array = $this->dailyb->get_active_bookings_for_hospital($device->hospital_id);
				$lessq_mail = $this->constants->get_constant('lessq_mail')->value;
				$lessq_tel_tech_support = $this->constants->get_constant('lessq_tel_tech_support')->value;
				$dailyq_ucode = $this->get_dailyb_ucode();
				if($data_array) 
				{
					$response["success"] = 1;
					$response["hospital_id"] = $hospital->hospital_id;
					$response["hospital_name"] = $hospital->hospital_name;
					$response["hospital_type"] = $hospital->type;
					$response["qucode"] = $dailyq_ucode;
					$response["lessq_tel_tech_support"] = $lessq_tel_tech_support;
					$response["lessq_mail"] = $lessq_mail;
					$response["data"] = $data_array;
					
				}else {
					$response["success"] = 0;
					$response["msg"] = "No any active records in the server";
				}
			}
			else
			{
				$response["success"] = 0;
				$response["msg"] = "Invalid passcode or unauthorized device";
			}
		}
		else
		{
			$response["success"] = -1;
			$response["msg"] = "Un registered device ID: ".$uid.". Please contact lessq help team.";
		} 

		echo json_encode($response);
	}

	//method is directly called by device in time intervals
	public function new_booking(){
		$response = null;
		if($this->verify_device())
		{
			if($this->verify_dailyb_ucode()) 
			{
				$dailyb_id = $this->input->post('bid');
				
				if($dailyb_id != -1) 
				{
					$nappo = $this->dailyb->add_booking($dailyb_id);
					$reference = $this->add_booking_reference($dailyb_id, $nappo, 0);
					$status = $this->dailyb->get_que_booking_status($dailyb_id);
					$response["success"] = 1;
					$response["operation"] = 'booking';
					$response["que_status"] = $status->que_status;
					$response["seeing_patient"] = $status->seeing_patient;
					$response["nappo"] = $nappo;
					$response["bref"] = str_pad($reference, 5, '0', STR_PAD_LEFT); 
				}
				else
				{
					$response["success"] = 0;
					$response["msg"] = "Internal problem. The requested shift is not available in the server";
					// $this->log_dailyq_calls($dailyq_id, 200);
				}
			}
			else
			{
				$response["success"] = 0;
				$response["msg"] = "Session expired. Start from begining";
			}
		}
		else
		{
			$response["success"] = 0;
			$response["msg"] = "Invalid passcode or unauthorized device";
		}
		echo json_encode($response);

	}

	function get_current_status()
	{
		$response = null;
		if($this->verify_device())
		{
			if($this->verify_dailyb_ucode()) 
			{
				$dailyb_id = $this->input->post('bid');

				if($dailyb_id != -1) 
				{
					$status = $this->dailyb->get_que_booking_status($dailyb_id);
					$response["success"] = 1;
					$response["operation"] = 'qstatus';
					$response["que_status"] = $status->que_status;
					$response["seeing_patient"] = $status->seeing_patient;
					$response["nappo"] = $status->nappo;
				}
				else
				{
					$response["success"] = 0;
					$response["msg"] = "Internal problem. The requested bookin is not available in the server";
					$this->log_dailyq_calls($dailyq_id, 200);
				}
			}
			else
			{
				$response["success"] = 0;
				$response["msg"] = "Session expired. Start from begining";
			}
		}
		else
		{
			$response["success"] = 0;
			$response["msg"] = "Invalid passcode or unauthorized device";
		}
		echo json_encode($response);
	}

	/********************** END Region BookingAPP functions *****************/

	/************************************************************************/
	/********************** Supporting functions **********************/
	/************************************************************************/

	private function verify_device(){
		$passcode = $this->input->post('passcode');
		$uid = $this->input->post('uid');
		// $passcode = '0011';
		// $uid = 'fa32-4720-4f82-a41f';
		$device = $this->device->verify($uid, $passcode);
		return $device;
	}

	private function verify_dailyb_ucode()
	{
		$qucode = $this->input->post('qucode');
		if($qucode == $this->get_dailyb_ucode())
			return true;
		else
			return false;
		
	}

//todo: redo the log
	private function add_booking_reference($dailyb_id, $nappo, $type)
    {
        $dailyq = $this->dailyb->get_dailyq_by_dailyb_id($dailyb_id);
        $shift_type = $dailyq->shift_type;
        $shift_special_id = $dailyq->shift_special_id;
        $shift_regular_id = $dailyq->shift_regular_id;
        $reference = $this->dailyb_log->insert($shift_type, $shift_special_id, $shift_regular_id, $nappo, $type);
        return $reference;
    }

// This function makes unique int from the dailyq date property
    public function get_dailyb_ucode()
    {
    	$ucode = $this->dailyb->get_date_for_ucode()->date;//format YYYYMMDD
    	return $ucode;
    }
    /********************** END supporting functions *****************/
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */