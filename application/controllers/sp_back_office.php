<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_back_office extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}

   		$this->load->model('dailyq','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('shift','',TRUE);
   		$this->load->model('sms_log','',TRUE);
   		$this->load->model('dailyq_log','',TRUE);
   		$this->load->model('doctor','',TRUE);
   		$this->load->model('sms_code','',TRUE);
   		$this->load->model('device','', TRUE);
   		$this->load->model('dailyb','', TRUE);
   		$this->load->model('dailyb_log','', TRUE);
 	}

 	public function index()
	{
		if(getenv('APPLICATION_ENV') == 'production')
        {
            $title = 'Back Office ';
        }
        else if(getenv('APPLICATION_ENV') == 'staging')
        {
            $title = 'Back Office-Staging';
        }
        else 
        {
            $title = 'Back Office-Local';
        }
        $data['title'] = $title;
		$this->load->view('sp_back_office', $data);
	}

  public function generate_device_uid()
  {
    $guid = '';
    do 
    {
      $guid = sprintf( '%04x-%04x-%04x-%04x',
          mt_rand( 0, 0xffff ), 
          mt_rand( 0, 0xffff ),
          mt_rand( 0, 0x0fff ) | 0x4000,
          mt_rand( 0, 0x3fff ) | 0x8000,
          mt_rand( 0, 0xffff ));
    }
    while($this->device->check_uid($guid));
      //check the values with db
    echo($guid);
  }

  public function display_devices()
  {
    $data['devices'] = $this->device->get_devices();
    $this->load->view('sp_display_devices', $data);
  }


	/*************************** End Region AJAX calls ***********************/




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */