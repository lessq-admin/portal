<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_help_desk extends CI_Controller {
    
	function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
        	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}

   		$this->load->model('dailyq','',TRUE);
   		$this->load->model('hospital','',TRUE);
   		$this->load->model('shift','',TRUE);
   		$this->load->model('sms_log','',TRUE);
   		$this->load->model('dailyq_log','',TRUE);
   		$this->load->model('doctor','',TRUE);
   		$this->load->model('sms_code','',TRUE);
   		$this->load->model('device','', TRUE);
   		$this->load->model('dailyb','', TRUE);
   		$this->load->model('dailyb_log','', TRUE);
      $this->load->library('utils');
 	}

 	public function index()
	{
		if(getenv('APPLICATION_ENV') == 'production')
    {
        $title = 'Help Desk';
    }
    else if(getenv('APPLICATION_ENV') == 'staging')
    {
        $title = 'LessQ Help Desk-Staging';
    }
    else 
    {
        $title = 'Help Desk-Local';
    }
    $data['title'] = $title;
		$this->load->view('sp_help_desk', $data);
	}
  /************************************************************************/
  /*************************** Region UI calls ****************************/
  /************************************************************************/

  public function add_temporary_shift()
  {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--Select--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    //make dummay doctor list
    $doctor_list = array();
    $doctor_list['0'] = '--All--'; 
    $override_list = array();
    $override_list['0'] = '--No--';
    $data['doctor_list'] = $doctor_list;
    $data['override_list'] = $override_list;
    $data['date'] = date('Y-m-d', time());

    $this->load->view('sp_add_temporary_shift', $data);
  }

  function add_temporary_shift_confirm()
  {
    //check password
    if($this->input->post('txt_password') != 'b@db0y') 
    {
      $this->session->set_flashdata('msg', 'Invalid password <br>');
      redirect(site_url('sp_help_desk/add_temporary_shift'), 'refresh'); 
      return;
    }

    $hospital_id = $this->input->post('ddl_hospitals');
    $doctor_id = $this->input->post('ddl_doctors');
    $date = $this->input->post('txt_date');
    $start_time = $this->input->post('ddl_start_time');
    $end_time = $this->input->post('ddl_end_time');

    $ddl_override = preg_split('/[_]/', $this->input->post('ddl_override'));
    $override_shift_id = $ddl_override[0]; 
    $override_shift_type = $ddl_override[1]; 

    $sms_code_id = $this->sms_code->get_sms_code($hospital_id, $doctor_id)->id;

    $curr_date = date('Y-m-d', time());

    $msg = '';
    $validate = true;

    //validation of inputs
    if($hospital_id <= 0)
    {
      $msg = 'Invalid hospital ';
      $validate = false;
    }
    if($doctor_id <= 0)
    {
      $msg = 'Invalid doctor ';
      $validate = false;
    }
    if(!validate_date($date, 'Y-m-d'))//check valid date)
    {
      $msg = 'Invalid date ';
      $validate = false;
    }
    if($override_shift_id < 0)
    {
      $msg = 'Invalid override queue ID ';
      $validate = false;
    }

    //check for overlap times
    $shift_array = $this->utils->get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
    $is_overlapped = false;
    
    foreach($shift_array as $row) 
    {
      if($override_shift_id > 0 && $row->type == $override_shift_type && $row->shift_id == $override_shift_id)
      {
        // echo "ssss";
        continue;
      }
    
      if(intval($row->status) == 1 && 
        (
          ((strtotime($end_time) >= strtotime($row ->start_time)) && (strtotime($end_time) <= strtotime($row ->end_time))) 
          || 
          ((strtotime($start_time) <= strtotime($row ->start_time)) && (strtotime($end_time) >= strtotime($row ->end_time)))
          ||
          ((strtotime($start_time) >= strtotime($row ->start_time)) && (strtotime($start_time) <= strtotime($row ->end_time)))
          ) 
        )
      {
        // echo '<br> Caught:'.$start_time.'~'.$end_time;
        $is_overlapped = true;
      } 
    }

    //verifies overlaps of staart and end time
    if($is_overlapped)
    {
      $msg = 'Start and end times are overlapped with the existing shift';
      $validate = false;
    }

    if(!$validate)
    {
      //$this->add_temporary_shift($msg);
      $this->session->set_flashdata('msg', 'Error occured <br>'.$msg);
      redirect(site_url('sp_help_desk/add_temporary_shift'), 'refresh'); 
      return;
    }
    // general validations compalted
    //Format start and end time
    $start_time = date('0000-00-00 H:i:s', strtotime($start_time));
    $end_time = date('0000-00-00 H:i:s', strtotime($end_time));
    if($override_shift_id == 0) //tempory shift
    {
      //formatting input data 
      $booking_enabled = 0;
      $booking_start_time = '00:00:00';
      $booking_end_time = '00:00:00';
      $booking_config_id = NULL;

      $shift_special_id = $this->shift->add_special_shift($date, $start_time, $end_time, $sms_code_id, 
        $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
      if($curr_date == $date)
      {
        $this->dailyq->add_special_shift($shift_special_id, $date, $start_time, $end_time, $sms_code_id, $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
      }
    }
    else
    {
      if($override_shift_type == 'Special') 
      {
        $shift_special = $this->shift->get_special_shift($override_shift_id);
        $booking_enabled = $shift_special->booking_enabled;
        $booking_start_time = $shift_special->booking_start_time;
        $booking_end_time = $shift_special->booking_end_time;
        $booking_config_id = $shift_special->booking_config_id;

        $shift_special_id = $this->shift->add_special_shift($date, $start_time, $end_time, $sms_code_id, 
          $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
        //change the status of special shift to 2
        $cancel_shift_id = $this->shift->shift_special_set_status($override_shift_id, 2);
        //update dailyq for current date
        if($curr_date == $date)
        {
          $dailyq = $this->dailyq->queue_for_shift_special($override_shift_id);
          $this->dailyq->dissable_queue($dailyq->id);
          $this->dailyq->add_special_shift($shift_special_id, $date, $start_time, $end_time, $sms_code_id, $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
        }
        
      }
      else if($override_shift_type == 'Regular')
      {
        //add cancel shift with status 2
        $shift_regular = $this->shift->get_regular_shift($override_shift_id);
        $booking_enabled = $shift_regular->booking_enabled;
        $booking_start_time = $shift_regular->booking_start_time;
        $booking_end_time = $shift_regular->booking_end_time;
        $booking_config_id = $shift_regular->booking_config_id;
        //adding cancel shift entry
        $cancel_shift_id = $this->shift->cancel_regular_shift($override_shift_id, $date);
        $this->shift->shift_cancel_set_status($cancel_shift_id, 2);//status override 
        //adding special shift
        $shift_special_id = $this->shift->add_special_shift($date, $start_time, $end_time, $sms_code_id, 
          $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
        if($curr_date == $date)
        {
          //dissable the existing q
          $dailyq = $this->dailyq->queue_for_shift_regular($override_shift_id);
          $this->dailyq->dissable_queue($dailyq->id);
          $this->dailyq->add_special_shift($shift_special_id, $date, $start_time, $end_time, $sms_code_id, $booking_enabled, $booking_start_time, $booking_end_time, $booking_config_id);
        }    
      }
    }

    $this->session->set_flashdata('msg', 'Shift insertion completed');
    redirect(site_url('sp_help_desk/add_temporary_shift'), 'refresh'); 
  }

  function cancel_shift()
  {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--Select--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $data['date'] = date('Y-m-d', time());
    $doctor_list = array();
        $doctor_list[''] = '--All--'; 
    $data['doctor_list'] = $doctor_list;
        $this->load->view('sp_cancel_shift', $data);
  }

  function cancel_shift_confirm()
  {
    //check password
    if($this->input->post('txt_password') != 'b@db0y') 
    {
      $this->session->set_flashdata('msg', 'Invalid password <br>');
      redirect(site_url('sp_help_desk/cancel_shift'), 'refresh'); 
      return;
    }
    
    $hospital_id = $this->input->post('ddl_hospitals');
    $date = $this->input->post('txt_date');
    $curr_date = date('Y-m-d', time());
    $check_list = $this->input->post('check_list'); 
    // echo $date;
    if(!empty($check_list)) 
    {
      foreach($check_list as $check) 
      {
        $check_id_parts = explode("_", $check);
        $type = $check_id_parts[0];
        $shift_id = $check_id_parts[1];
        
        if($type == 'Regular')
        {
          $this->shift->cancel_regular_shift($shift_id, $date);
          if($curr_date == $date)
          {
            $this->dailyq->cancel_regular_shift($shift_id, $date);
          }
        }
        else if($type == 'Special')
        {
          $this->shift->cancel_special_shift($shift_id, $date);
          if($curr_date == $date)
          {
            $this->dailyq->cancel_special_shift($shift_id, $date);
          }
        }
      }
    }

    $this->session->set_flashdata('msg', 'Shift cancelation completed');
    redirect(site_url('sp_help_desk/cancel_shift'), 'refresh'); 
  }

  public function check_lessq_sms()
  {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--All--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $data['from_date'] = date('Y-m-d', time());
    $data['to_date'] = date('Y-m-d', time());
    $doctor_list = array();
        $doctor_list[''] = '--All--'; 
    $data['doctor_list'] = $doctor_list;
    $sms_status_list['-1'] = '--All--'; 
    $sms_status_list['1'] = 'Success'; 
    $sms_status_list['0'] = 'Failed'; 
    $data['sms_status'] = $sms_status_list;
    $data['sms_reply_enable'] = 0;
    $this->load->view('sp_hd_sms_reply', $data);
  }

  public function internal_sms_reply_search_results()
  {
    $hospital_id = $this->input->post('ddl_hospitals');
    $doctor_id = $this->input->post('ddl_doctors');
    $from_date = $this->input->post('from_date');
    $to_date = $this->input->post('to_date');
    $sms_status = $this->input->post('ddl_sms_status');

    $sms_codes = array();
    if($hospital_id != 0 && $doctor_id != 0)
    {
      $sms_code = $this->sms_code->get_sms_code($hospital_id, $doctor_id);
      $sms_codes[] = (object)$sms_code;
    }
    else if($hospital_id != 0) //doctor_id == 0
    {
      $res = $this->sms_code->get_sms_codes_for_hospital($hospital_id);
      foreach($res as $key => $sms_code)
      {
        $sms_codes[] = (object)$sms_code;
      }
    }

    $tbl_data = array();
    $res_array = array();
    if(count($sms_codes) > 0)
    {
      foreach ($sms_codes as $key => $row)
      { 
        $sms_code = $row->code;
        $res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
        foreach ($res as $key => $row)
        {
          $res_array[] = (object)$row;
        }
      }
    }
    else
    {
      $sms_code = '';
      $res = $this->sms_log->get_sms_for_query($sms_code, $from_date, $to_date, $sms_status);
      foreach ($res as $key => $row)
      {
        $res_array[] = (object)$row;
      }
    }

    $cnt = 0;
    foreach ($res_array as $key => $val)
    {
      $request = $val->request;
      $response = $val->response;
      $result = $val->result;
      $obj_req = json_decode($request);
      $obj_res = json_decode($response);
      $obj_confirm = json_decode($result);
      $req_msg = $obj_req->message;
      $res_msg = $obj_res->message;
      $con_msg = '';
      if($obj_confirm)
        $con_msg = $obj_confirm->destinationResponses[0]->statusCode.':'.$obj_confirm->destinationResponses[0]->statusDetail;
      // if(strlen($obj_req->message) > 12)
      //   $req_msg = substr($obj_req->message, 0, 20).'...'; //
      // else
      //   $req_msg = $obj_req->message;
      // $res_msg = substr($obj_res->message, 0, 60).'...'; 
      $tel     = '...'.substr($obj_req->sourceAddress, -5); 
      $date_time = $val->request_time;
      $carrier = $val->carrier;
      $id = $val->id;
      $row = array(
        'no' => $cnt++,
        'log_id' => $id,
        'carrier' => $carrier,
        'date_time' => $date_time,
        'tel' => $tel,
        'req_msg' => $req_msg,
        'res_msg' => $res_msg,
        'con_msg' => $con_msg
      );
      $tbl_data[] = (object)$row;
    }

    //preparing display data
    if($hospital_id == 0)
      $selected_hospital = '--All--';
    else
      $selected_hospital = $this->hospital->get_hospital($hospital_id)->hospital_name;
    if($doctor_id == 0)
      $selected_doctor = '--All--';
    else
      $selected_doctor = $this->doctor->get_doctor($doctor_id)->display_name;
    $sms_status_list['-1'] = '--All--'; 
    $sms_status_list['1'] = 'Success'; 
    $sms_status_list['0'] = 'Failed'; 
    // echo $sms_status; return;
    $selected_sms_status = $sms_status_list[$sms_status];

    $data['selected_hospital'] = $selected_hospital;
    $data['selected_doctor'] = $selected_doctor;
    $data['selected_sms_status'] = $selected_sms_status;
    $data['selected_from_date'] = $from_date;
    $data['selected_to_date'] = $to_date;
    $data['tbl_data'] = $tbl_data;

    $this->load->view('sp_hd_sms_reply_result', $data);
  }

  public function sms_reply_back_confirm()
  {

    //check password
    if($this->input->post('txt_password') != 'b@db0y') 
    {
      $this->session->set_flashdata('msg', 'Invalid password <br>');
      //redirect(site_url('sp_help_desk/check_lessq_sms'), 'refresh');
      redirect(site_url('sp_help_desk/internal_sms_reply_search_results'), 'refresh'); 
      
      return;
    }

    $sms_text = $this->input->post('txt_sms');
    $check_list = $this->input->post('check_list'); 

    $list = print_r($check_list, true);
    log_message('info', $list);
    log_message('info', $sms_text);
    // echo count($check_list);
    // echo $date;
    if(!empty($check_list)) 
    {
      foreach($check_list as $log_id) 
      {
        // echo $log_id;
        $sms_log = $this->sms_log->get_sms_log_for_id($log_id);
        $carrier = $sms_log->carrier;
        $response = $sms_log->response;
        $obj_res = json_decode($response);
        if($carrier == 'dialog')
          $senderURL = SENDER_URL_DIALOG;//'https://localhost:7443/sms/send';
        if($carrier == 'etisalat')
          $senderURL = SENDER_URL_ETISALAT;//'https://localhost:7443/sms/send';

        $arrayField = array("applicationId" => $obj_res->applicationId,
                "password" => $obj_res->password,
                "message" => $sms_text,
                "destinationAddresses" => $obj_res->destinationAddresses);
            $jsonObjectFields = json_encode($arrayField, JSON_UNESCAPED_SLASHES);
            //TODO:make log in database

            $ch = curl_init($senderURL);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonObjectFields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            curl_close($ch);
      }
      $this->session->set_flashdata('msg', 'SMS reply completed');
    }
    else
    {
      $this->session->set_flashdata('msg', 'Error occured </br> Start from begining');
    }
    redirect(site_url('sp_help_desk/check_lessq_sms'), 'refresh');
  }

  function add_special_message() {
    $hoslpital_obj_list = $this->hospital->get_all_hospitals();
    $hoslpital_list['0'] = '--Select--'; 
    if($hoslpital_obj_list) {
      foreach($hoslpital_obj_list as $row) {
        $hoslpital_list[$row->id] = $row->name;
      }
    }
    $data['hospital_list'] = $hoslpital_list;
    $doctor_list = array();
    $doctor_list[''] = '--All--'; 
    $data['doctor_list'] = $doctor_list;
    $data['date'] = date('Y-m-d', time());
    $this->load->view('sp_add_special_message', $data);
  }

  function add_doctors_message_confirm() {
    
  } 

  /*************************** End Region UI calls ***********************/

  /************************************************************************/
  /*************************** Region AJAX calls **************************/
  /************************************************************************/
  function json_get_shifts_for_hospital_doctor_date() 
  {
    $hospital_id = $this->input->post('ddl_hospitals');
    $doctor_id = $this->input->post('ddl_doctors');
    $date = $this->input->post('txt_date');
    $data_array = $this->utils->get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date);
    //check for cancelations
    foreach ($data_array as $row)
    {
      $shift_regular_id = $row->shift_id;
      $row->status = 1;
      $cancelled_shift = $this->shift->get_cancel_shifts($shift_regular_id, $date);
      // 0: inactive
      // 1: active
      // 2: override
      if($cancelled_shift)
      {
        if($cancelled_shift->status == 1)
          $row->status = 0;
        else if($cancelled_shift->status == 2)
          $row->status = 2;

      }
    }
    echo json_encode($data_array);
    // print_r($data_array);
    // echo $hospital_id;
  }

  function json_get_dailyq_for_hospital_doctor()
  {
    $hospital_id = $this->input->post('ddl_hospitals');
    $doctor_id = $this->input->post('ddl_doctors');
    $data_array = $this->utils->get_dailyq_for_hospital_doctor($hospital_id, $doctor_id);
    echo json_encode($data_array);
    
  }



  /*************************** End Region AJAX calls **********************/

  /************************************************************************/
  /*************************** Region Utils *******************************/
  /************************************************************************/

  // function get_shifts_for_hospital_doctor_date($hospital_id, $doctor_id, $date)
  // {
  //   //return status; 0:cancel, 1:active, 2:override
  //   $dateObj = DateTime::createFromFormat('Y-m-d', $date);
  //   $dayofweek = $dateObj->format('w');

  //   $regular_shifts = $this->shift->get_regular_shift_for_hospital_doctor_day_of_week($hospital_id, 
  //     $doctor_id, $dayofweek);
  //   $data_array = $regular_shifts;
  //   foreach ($data_array as $row)
  //   {
  //     $shift_regular_id = $row->shift_id;
  //     $cancelled_shifts = $this->shift->get_cancel_shifts($shift_regular_id, $date);
  //     if($cancelled_shifts)
  //     {
  //       $row->status = $cancelled_shifts->status;
  //     }
  //     $row->type = 'reg';
  //   }
  //   //get special shifts
  //   $special_shifts = $this->shift->get_special_shift_for_hospital_doctor_date($hospital_id, 
  //     $doctor_id, $date);
  //   foreach ($special_shifts as $row)
  //   {
  //     $row->type = 'spe';
  //     $data_array[] = $row;
  //   }
  //   return $data_array;
  // }



  /*************************** End Region Utils ***************************/    


	/*************************** End Region AJAX calls ***********************/




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */