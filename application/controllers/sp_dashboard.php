<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sp_dashboard extends CI_Controller {
    
	function __construct()
 	{
 		parent::__construct();
      if (!isset($_SERVER['PHP_AUTH_USER']) || 
      	$_SERVER['PHP_AUTH_USER'] != 'lessqadmin' || 
      	$_SERVER['PHP_AUTH_PW'] != 'l355qsh00t') 
      {
    		header('WWW-Authenticate: Basic realm="Admin"');
    		header('HTTP/1.0 401 Unauthorized');
    		die('Access Denied');
  	}

 		$this->load->model('dailyq','',TRUE);
    $this->load->model('sms_code','',TRUE);
    $this->load->model('doctor','',TRUE);
    $this->load->model('hospital','',TRUE);
 	}

 	public function index()
	{
		if(getenv('APPLICATION_ENV') == 'production')
    {
      $title = 'Dashboard';
    }
    else if(getenv('APPLICATION_ENV') == 'staging')
    {
      $title = 'Dashboard-Staging';
    }
    else 
    {
      $title = 'Dashboard-Local';
    }
    $data['title'] = $title;
		$this->load->view('sp_dashboard', $data);
	}

  /************************************************************************/
  /*************************** Region UI calls ****************************/
  /************************************************************************/
  function get_que_details($que_id)
  {
    $data = array();
    $dailyq = $this->dailyq->get_dailyq($que_id);
    $sms_code = $this->sms_code->get_sms_code_for_id($dailyq->sms_code_id);
    $hospital = $this->hospital->get_hospital_info($sms_code->hospital_id);
    $doctor = $this->doctor->get_doctor_info($sms_code->doctor_id);
    $data['que_id'] = $que_id;
    $data['sms_code'] = $sms_code->code;
    $data['opr_time'] = date("h:i a", strtotime($dailyq->start_time))."~".date("h:i a", strtotime($dailyq->end_time));
    //status
    $dis_status_data = $this->get_display_status($dailyq);
    $data['disp_status'] = $dis_status_data['disp_status'];
    $data['status'] = $dailyq->status;
    $data['last_updated_time'] = date("h:i:s a", strtotime($dailyq->updated));
    $data['hospital'] = $hospital->name;
    $data['doctor'] = $doctor->display_name;
    $data['contact'] = $hospital->contact_person.", ".$hospital->contact_person_name."(".$hospital->phone1.")";
    $this->load->view('sp_dashboard_qdetails', $data);
  }
	/*************************** End Region UI calls ***********************/

  /************************************************************************/
  /*************************** Region AJAX calls **************************/
  /************************************************************************/
  function json_get_current_status()
  {
    $dailyqs = $this->dailyq->get_dailyqs();
    $curr_time = date("h:i:s", strtotime(get_current_time()));
    $data = array();
    $data['syncked'] = get_current_date_time();
    $siaply_data = array();
    foreach ($dailyqs as $key => $que)
    {
      $row = array();
      $sms_code = $this->sms_code->get_sms_code_for_id($que->sms_code_id);
      $id = $que->id;
      $start_time = $que->start_time;
      $end_time = $que->end_time;
      $updated_time = $que->updated;
      $patient_count = $que->patient_count;
      $status = $que->status;

      $row['id'] = $id;
      $row['sms_code'] = $sms_code->code;
      $row['opr_time'] = date("h:i a", strtotime($start_time))."~".date("h:i a", strtotime($end_time));
      $dis_status_data = $this->get_display_status($que);
      $row['status_bg']  = $dis_status_data['status_color'];
      $row['disp_status']  = $dis_status_data['disp_status'];
      $row['last_updated_time'] = date("h:i:s a", strtotime($updated_time));
      $display_data[] = $row;
    }
    $data['display_data'] = $display_data;

    $json_data= json_encode($data);
    echo $json_data;
  }

  function json_finish_queue()
  {
    $que_id = $this->input->post('que_id');
    $aff_rows = $this->dailyq->finish_queue($que_id);
    $data = array();
    if($aff_rows == 1)
    {
      $data['status'] = 1;
      $data['msg'] = 'Successfully finish the queue';
    }
    else
    {
      $data['status'] = 0;
      $data['msg'] = 'Operation failed. Try again !';
    }
    $json_data= json_encode($data);
    echo $json_data;
  }



  /*************************** End Region AJAX calls **********************/

  /************************************************************************/
  /*************************** Region Utils *******************************/
  /************************************************************************/
  function get_display_status($dailyq)
  {
    /*
        Status 
          Default: blue #809fff
          Started:green #99ff99
          late start:orange #ffa366
          sync failed:red #ff4d4d
          cancelld:greay #8c8c8c
          finished:dark green #009933
          eat:purple #b380ff
      */
    $data = array();
    $curr_time = date("h:i:s", strtotime(get_current_time()));
    $start_time = $dailyq->start_time;
    $status_color = "";
    $disp_status = "";
    switch ($dailyq->status) {
      case 0: //active
        $disp_status = "Not started";
        $status_color = "#809fff";
        if(strtotime($curr_time) > strtotime(date("h:i:s", strtotime($start_time))))
        {
          $disp_status = "Late start";
          $status_color = "#ffa366";//late start
        }
        break;
      case 1: //started
        $disp_status = "Seeing patient: ".$dailyq->patient_count;
        $status_color = "#99ff99";
        break;
      case 2: //finished
        $disp_status = "Finishid with patient count: ".$dailyq->patient_count;
        $status_color = "#006633";
        break;
      case 3: //eat
        $disp_status = "EAT(TODO)";
        $status_color = "#b380ff";
        break;
      case 4: //cancelld
        $disp_status = "Canceld shift at ". $dailyq->updated;
        $status_color = "#8c8c8c";
        break;
      default:       
    }
    $data['status_color'] = $status_color;
    $data['disp_status'] = $disp_status;
    return $data;
    
  }


  /*************************** End Region Utils ***************************/




}