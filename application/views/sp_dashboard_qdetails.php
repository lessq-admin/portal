
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<!-- script type="text/javascript" src="<php echo asset_url();?>js/jquery.alerts.js" -->
<script type="text/javascript">
$(document).ready(function() {
  $("#check_select_all").change(function(){
    if ($("#check_select_all").is(':checked')) {
      $("#table_div input[type=checkbox]").each(function () {
        $(this).prop("checked", true);
      });

    } else {
      $("#table_div input[type=checkbox]").each(function () {
        $(this).prop("checked", false);
      });
    }
  });

  if(<?php echo $status?> != 1)
    $('#btnFinishShift').prop('disabled', true);
  
  $("#btnFinishShift").click(function(){
    //TODO with JConfirm
    // jConfirm('Can you confirm this?', 'Confirmation Dialog', function(r) {
    // });
    if (confirm("Are you sure you want finish this shift ?")){
      cancelShift();
    } 
    
    return false;
  });

  $('#txt_sms').keyup(function () {
    var max = 160;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text(' you have reached the limit');
    } else {
      var char = max - len;
      $('#charNum').text(char + ' characters left');
    }
    if(len > 0 & $('#table_div input:checked').length > 0) {
      $("#btnSend").attr("disabled", false);
    }
    else {
      $("#btnSend").attr("disabled", true);
    }
  });

  $("#msgdiv").delay(2500).fadeOut('slow');
});

function cancelShift()
{
  var jsonData = $.ajax({
  url: "<?php echo site_url('sp_dashboard/json_finish_queue');?>",
  type: "POST",
  data: {'que_id':"<?php echo $que_id?>"} ,
  dataType:"json",
  async: false
  }).complete(function(){
      }).responseText;
  //alert(jsonData);
  // return;
  var data = JSON.parse(jsonData);
  //process on error messages
  if(data.error != null) {
    $("#msgdiv").empty();
    $("#msgdiv").append("Error has occured. </br> Please contact LessQ admin.");
    return;
  }
  $("#msgdiv").empty();
    $("#msgdiv").append(data.msg);
  if(data.status == 1)
    $('#btnFinishShift').prop('disabled', true);
  
}

</script>
<title>Queue Details</title>
</head>
<body>
<div style="color:#FF0000" id="msgdiv">
  <h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>
<h3>Queue Details</h3>
<div id="container">
    <div><?php echo $sms_code ?></div>
    <div><?php echo $opr_time ?></div>
    <div><?php echo $disp_status ?></div>
    <div>Last updated: <?php echo $last_updated_time ?></div>
    <div><?php echo $hospital ?></div>
    <div><?php echo $doctor ?></div>
    <div><?php echo $contact ?></div>
    </br>
    <div>
    <?php
      $data = array(
      'id' => 'btnFinishShift',
      'type' => 'submit',
      'value'=> 'Finish shift',
      'class'=> 'submit',
      );
      echo form_submit($data);   
    ?>
    </div>
    </br>
    </br>
    <!--
    <div> SMS message <span id="charNum">160 characters left</span></div>
    <php
    $data = array(
          'name'        => 'txt_sms',
          'id'          => 'txt_sms',
          'rows'        => '8',
          'cols'        => '10',
          'maxlength'   => '160',
          'style'       => 'width: 500px;',
        );

      echo form_textarea($data);
    ?>
    <div><input type="button" id="btn_sms_admin" value="Send SMS to Admin"/></div> -->
</div>
</body>
</html>