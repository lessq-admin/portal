
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
	$(function() {
    		$("#msgdiv").delay(2500).fadeOut('slow');
	});
</script>
<title><?php echo $title?></title>
</head>
<body>
<div><h1><?php echo $title?></h1></div>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>
<div><h2>Device operational testing</h2></div>
<?php echo form_open('sp_operation/change_operational_device_setting'); ?>
<div>
	<div>
		<h4>UID: <?php echo $device_uid?> </h4>
	</div>
	<div>
		<h4>Passcode: <?php echo $passcode?> </h4>
	</div>
	<div>
		<h4>Description: <?php echo $description?> </h4>
	</div>
	<div>
		<label for="ddl_hospitals">Select Hospital:</label>
		<?php  echo form_dropdown('ddl_hospitals', $hospital_list, $ddl_hospital, 'id="ddl_hospitals"');?>
	</div>
	</br>
	<div>
		<label for="ddl_status">Status:</label>
		<?php  echo form_dropdown('ddl_status', $status_list, $ddl_status, 'id="ddl_status"');?>
	</div>
	</br>
	<div>
		<?php
			// Show Name Field in View Page
			$data= array(
			'name' => 'txt_password',
			'class' => 'input_box'
			);
			echo 'Operation authentication password: ';echo form_password($data);
		?>
	</div>
	</br>
	<div>
		<?php
			$data = array(
			'type' => 'submit',
			'value'=> 'Authenticate',
			'class'=> 'submit'
			);
			echo form_submit($data); 
		?>
	</div>
</div>
<?php echo form_close(); ?>
</body>
</html>

