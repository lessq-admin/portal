
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});

$(document).ready(function() {
    $("#txt_date").datepicker({ dateFormat: 'yy-mm-dd', maxDate: '0' });
    $("#ddl_hospitals").change(function () 
        {
            setToInit();
            if($('#ddl_hospitals').val() == 0)
                $( "#txt_date" ).prop( "disabled", true );
            else
                $( "#txt_date" ).prop( "disabled", false );
        });
    $('#txt_date').bind('change', load_charts);
});

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}

function setToInit()
{
    setMsgDiv("");
    document.getElementById('charts_div').innerHTML = '';
    $('#txt_date').text('--');
}

function load_charts() 
{ 
    setMsgDiv("");
    document.getElementById('charts_div').innerHTML = '';

    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_q_operation_log');?>",
        type: 'POST',
        data: { 'hospital_id': $("#ddl_hospitals").val(), 'date' : $("#txt_date").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    // alert(jsonData);
    // return;
    var jsonArry = JSON.parse(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        
        var options = {
            title: jsonObj.doctor_name + ' on ' + jsonObj.date + ' (' + jsonObj.shift_type + ' shift): ' + jsonObj.start_time + '~' + jsonObj.end_time,
            width: 900,
            height: 300,
            legend: 'none',
            vAxis: { gridlines: { count: 10 }},
            hAxis: {gridlines: { count: 100 }}
        };
        var table = new google.visualization.DataTable(jsonObj.table);
        var innerDiv = document.createElement('div');
        document.getElementById('charts_div').appendChild(innerDiv);
        var chart = new google.visualization.LineChart(innerDiv);
        chart.draw(table, options);
      
    }
}

</script>
<title>Q Operation Logs</title>

</head>
<body>
<div>
<h2>Select your search criteria</h2>
</div>
<div>
<?php echo form_open('internal/q_operation_logs'); ?>
<label for="ddl_hospitals">Hospital:</label>
<?php echo form_dropdown('ddl_hospitals', $hospital_list,'', 'id="ddl_hospitals"');?>
</br>
</br>
<label for="from_date">Select date:</label>
<?php 
$data = array(
              'name'        => 'txt_date',
              'id'          => 'txt_date',
              'value'       => '--',
              'size'        => '10',
              'disabled'    => true,
              'required' => 'required');
echo form_input($data);
?>
<?php echo form_close(); ?>
</br>
</br>

</div>
<div style="color:#FF0000" id="msgdiv"></div>
<div id="charts_div"></div>
</body>
</html>

