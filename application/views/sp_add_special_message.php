
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="<?php echo asset_url() ?>js/jquery-1.11.3.min.js"></script>
  <script src="<?php echo asset_url() ?>js/jquery.tablesorter.js"></script>
  
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

  <link rel="stylesheet" href="<?php echo asset_url() ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo asset_url() ?>css/theme.blue.css">

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> -->
<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"> -->

<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {  
  $("#ddl_hospitals").on('click', 'li a', function()
  {

    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
  $(this).parents(".dropdown").find('.btn').val($(this).data('value'));

    // $("#btn_ddl_hospital").val($(this).text());
    //   $("#btn_ddl_hospital").text($(this).text());

    alert(3);
      setToInit();
      loadDoctors();
      load_table();
      if($('#ddl_hospitals').val() == 0)
      {
          //$( "#txt_date" ).prop( "disabled", true );
          resetDoctors();
      }
      else
      {
         // $( "#txt_date" ).prop( "disabled", false );
      }
  });

  $("#ddl_doctors").change(function () 
  {
    load_table();
  });

  $('#from_add_special_message').on('change', ':checkbox', function () {
    var clicked = $(this);
    //alert(clicked.val());
    $("input[name='check_list[]']").each( function () {
      if($(this).val() != clicked.val())
        $(this).attr("checked",false);
   });
  });


  $('#txt_sms').keyup(function () {
    var max = 160;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text(' you have reached the limit');
    } else {
      var char = max - len;
      $('#charNum').text(char + ' characters left');
    }
    if(len > 0 & $('#table_div input:checked').length > 0) {
      $("#btnAdd").attr("disabled", false);
    }
    else {
      $("#btnAdd").attr("disabled", true);
    }

  });

});

function loadDoctors() 
{
    setMsgDiv("");
    $("#ddl_doctors").prop("disabled", false);
    if($('#ddl_hospitals').val() == 0)
      return;
    document.getElementById('table_div').innerHTML = '';
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_get_doctors_for_hospital');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }
        
    var ddl_doctors = $("#ddl_doctors");
    ddl_doctors.empty();
    $.each(jsonArry, function() {
        ddl_doctors.append($("<option />").val(this.doctor_id).text(this.display_name));
        //alert(this.display_name);
    });
}

function load_table() 
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';

    if($("#ddl_hospitals").val() == 0 || !Date.parse($("#txt_date").val()))
    {
      return;
    }
    var jsonData = $.ajax({
        url: "<?php echo site_url('sp_help_desk/json_get_dailyq_for_hospital_doctor');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val(), 'ddl_doctors':  $("#ddl_doctors").val()} ,
        dataType:"json",
        async: false
        }).responseText;
        console.log(jsonData);
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }

    table_data = '<table class="table table-striped">';
    table_data += '<tr><th></th><th>No</th><th>Doctor</th><th>Start</th><th>End</th><th>Type</th><th>Status</th><th>Booking</th><th>Message</th></tr>';

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        no = i + 1;
        qid = jsonObj.id;
        doctor_name = jsonObj.display_name;
        start_time = jsonObj.start_time;
        end_time = jsonObj.end_time;
        type = jsonObj.type;
        status = jsonObj.status;
        shift_id = jsonObj.shift_id;
        if(jsonObj.booking_enabled == 1)
          booking_enabled = 'Yes';
        else
          booking_enabled = 'No';
        special_msg = '';
        if(jsonObj.special_msg)
          special_msg = jsonObj.special_msg;

        //shift status
        // 0: (default) not stated
        // 1: started
        // 2: finish
        // 3: eat
        // 4: cancel
        // 5: standby
        // 6: dissabled
        // 7: (default) not stated with message
        var status_str = '';
        if(status == 0)
          status_str = 'Not started';
        else if(status == 1)
          status_str = 'Started';
        else if(status == 2)
          status_str = 'Finished';
        else if(status == 3)
          status_str = 'Expected Time';
        else if(status == 4)
          status_str = 'Cancel';
        else if(status == 5)
          status_str = 'Stand by';
        else if(status == 6)
          status_str = 'Dissabled';
        else if(status == 7)
          status_str = 'Messaged';

        table_data += '<tr>';
        if(status == 0 || status == 7) 
        {
          table_data += '<td><input type="checkbox" id="check_list" name="check_list[]"  value="' + type + '_' + qid + '">';
        }
        else
        {
          table_data += '<td></td>';
        }
        table_data += '<td>'+no+'</td><td>'+doctor_name+'</td><td>'+start_time+'</td><td>'+end_time+'</td><td>'+type+'</td><td>'+status_str+'</td><td>'+ booking_enabled +'</td><td>'+ special_msg + '</td>';

        table_data += '</tr>';
    }
    table_data += '</table>';
    document.getElementById('table_div').innerHTML = table_data;
}
function setToInit()
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}


</script>
<title>Add Special message</title>

</head>
<body>

<div style="color:#FF0000" id="msgdiv">
  

<div class="container">
<h3><?php
   $msg = $this->session->flashdata('msg');
   if(isset($msg)) echo $msg; 
   ?></h3>
</div>
<div>
<h2>Select the shift and cancel</h2>
</div>



    <form role="form" class="form-horizontal">
        <div class="form-group col-sm-5">
            <label for="inputPassword" class="col-xs-1 control-label">Date</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" id="inputPassword" placeholder="Password" />
            </div>
        </div>
        <div class="form-group col-sm-5">
            <label for="inputPassword" class="col-xs-4 control-label">Password</label>
            <div class="col-xs-8">
                <input type="password" class="form-control" id="inputPassword" placeholder="Password" />
            </div>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>




<div>
<?php 
$data = array(
              'id' => 'from_add_special_message');
// echo form_open('sp_help_desk/add_special_message', $data); ?>
<form class="form-inline" id="from_add_special_message">
<!-- <div class="col-xs-3">
  <label for="txt_date">Date:</label>
  <input type="text" class="form-control" id="txt_date" disabled="false"  value=<?echo $date?> >
</div -->

<label for="rg-from">Ab: </label>
  <div class="form-group">
    <input type="text" id="rg-from" name="rg-from" value="" class="form-control">
  </div>

<label for="date">Date:</label>
<?php 
$data = array(
              'name'        => 'txt_date',
              'id'          => 'txt_date',
              'value'       => $date,
              'size'        => '10',
              'disabled'    => true,
              'required' => 'required');
//echo form_input($data);
?>
</br>
</br>
<label for="ddl_hospitals">Hospital:</label>
<?php echo form_dropdown('ddl_hospitals1', $hospital_list,'', 'id="ddl_hospitals1"');?>
</br>
<div class="dropdown">
  <button class="btn dropdown-toggle" type="button" id="btn_ddl_hospital" data-toggle="dropdown">Dropdown Example
  <span class="caret"></span></button>
  <ul id="ddl_hospitals" class="dropdown-menu">
  <?php
  foreach($hospital_list as $hospital) {
    echo '<li><a href="#">'.$hospital.'</a></li>';
  }
  ?>
  </ul>
</div>
</br>
<label for="ddl_doctors">Doctor:</label>
<?php echo form_dropdown('ddl_doctors', $doctor_list,'', 'id="ddl_doctors" disabled="disabled"');?>
</br>
</br>
<div id="table_div"></div>
</div>
</br>
<div> SMS message <span id="charNum">160 characters left</span></div>
<?php

$data = array(
      'name'        => 'txt_sms',
      'id'          => 'txt_sms',
      'rows'        => '8',
      'cols'        => '10',
      'maxlength'   => '160',
      'style'       => 'width: 500px;',
    );

  echo form_textarea($data);
?>
</br>
</br>
<?php
  $data = array(
  'id' => 'btnAdd',
  'type' => 'submit',
  'value'=> 'Add',
  'class'=> 'submit',
  'disabled'    => true
  );
  echo form_submit($data);   
?>
<?php echo form_close(); ?>
</div>
</body>
</html>

