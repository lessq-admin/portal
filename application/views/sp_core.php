
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="<?php echo asset_url() ?>js/jquery.confirm.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>

<script type="text/javascript">
	$(function() {
    		$("#msgdiv").delay(2500).fadeOut('slow');
	});

	$(document).ready(function(){ 

	$("#btnResetQB").confirm({
	    title:"Reset confirmation",
	    text:"This is very dangerous, you shouldn't do it! Are you really really sure?",
	    confirm: function(button) {
	        window.location.href = "<?php echo site_url('sp_core/reset_daily_qb'); ?>";
	    },
	    cancel: function(button) {
	    },
	    confirmButton: "Yes I am",
	    cancelButton: "No"
	});

	$("#btnInternalSMS").click(function(){
        window.location.href = "<?php echo site_url('sp_core/internal_sms_reply'); ?>";
    }); 

    $("#btnSMSLogs").click(function(){
        window.location.href = "<?php echo site_url('sp_core/sms_reply_log'); ?>";
    }); 
});

</script>
<style type="text/css">
	ul
	{
	    list-style-type: none;
	}
	.btn{
		margin:10px;
	}
</style>
<title><?php echo $title?></title>
</head>
<body>
<h1><?php echo $title?></h1>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>

<div>
	<ul>
	  <li><button class="btn btn-primary" id="btnResetQB">Reset Daily Que & Bookings</button></li>
	  <li><button class="btn btn-primary" id="btnInternalSMS">Internal SMS reply</button></li>
	  <li><button class="btn btn-primary" id="btnSMSLogs">SMS logs</button></li>
	</ul>

</div>
</body>
</html>