
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function() {
	$("#to_date").datepicker({ 
    dateFormat: 'yy-mm-dd', minDate: $.datepicker.parseDate("yy-mm-dd", $("#from_date").val())}
  );
  $("#from_date").datepicker({ dateFormat: 'yy-mm-dd', maxDate: '0'  }).bind("change",function(){
      var minValue = $(this).val();
      minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
      minValue.setDate(minValue.getDate()+1);
      $("#to_date").datepicker( "option", "minDate", minValue );
  });
});
</script>
<title>SMS filter</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>
<div>
<h2>Select your search criteria</h2>
</div>
<div>
<?php echo form_open('internal/sms_filter_result'); ?>
<label for="from_date">Date from:</label>
<?php 
$data = array(
              'name'        => 'from_date',
              'id'          => 'from_date',
              'value'       => $min_date,
              'size'        => '10',
              'required' => 'required');
echo form_input($data);
?>
<label for="to_date">To:</label>
<?php 
$data = array(
              'name'        => 'to_date',
              'id'          => 'to_date',
              'value'       => $max_date,
              'size'        => '10',
              'required' => 'required');
echo form_input($data);
?>
</br>
</br>
<?php
	$data = array(
	'type' => 'submit',
	'value'=> 'Submit',
	'class'=> 'submit'
	);
	echo form_submit($data); 

	
?>

</div>
</body>
</html>

