
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function() {
	$("#txt_date").datepicker({ dateFormat: 'yy-mm-dd', minDate: '0' });
  $('#txt_date').bind('change', load_table);  
  $('#txt_date').bind('change', activateSubmit());  
  $("#ddl_hospitals").change(function () 
  {
      setToInit();
      loadDoctors();
      load_table();
      if($('#ddl_hospitals').val() == 0)
      {
          $( "#txt_date" ).prop( "disabled", true );
          resetDoctors();
      }
      else
      {
          $( "#txt_date" ).prop( "disabled", false );
      }
      activateSubmit();
  });

  $("#ddl_doctors").change(function () 
  {
      activateSubmit();
      load_table();
  });

  $("#ddl_start_time").change(function () 
  {
      activateSubmit();
  });
  $("#ddl_end_time").change(function () 
  {
      activateSubmit();
  });
});

function activateSubmit()
{
  // console.log($("#ddl_doctors").val());
  // console.log(Date.parse("1-1-2000 " + $("#ddl_start_time").val()));
  if(Date.parse("1-1-2000 " + $("#ddl_start_time").val()) < Date.parse("1-1-2000 " + $("#ddl_end_time").val())) 
  {
    $("#btnRegister").attr("disabled", false);
  }
  else
  {
    $("#btnRegister").attr("disabled", true);
  }
}

function resetDoctors()
{
    $("#ddl_doctors").prop("disabled", true);
}

function loadDoctors() 
{
    setMsgDiv("");
    $("#ddl_doctors").prop("disabled", false);
    if($('#ddl_hospitals').val() == 0)
      return;
    document.getElementById('table_div').innerHTML = '';
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/get_doctors_for_hospital');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }
        
    var ddl_doctors = $("#ddl_doctors");
    ddl_doctors.empty();
    $.each(jsonArry, function() {
        ddl_doctors.append($("<option />").val(this.doctor_id).text(this.display_name));
        //alert(this.display_name);
    });
}

function load_table() 
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
    if($("#ddl_hospitals").val() == 0 || !Date.parse($("#txt_date").val()))
    {
      return;
    }
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_get_shifts_for_hospital_doctor_date');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val(), 'ddl_doctors':  $("#ddl_doctors").val(), 'txt_date' : $("#txt_date").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }

    table_data = '<table>';
    table_data += '<tr><th>No</th><th>Doctor</th><th>Start</th><th>End</th><th>Type</th><th>Status</th></tr>';

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        no = i + 1;
        doctor_name = jsonObj.display_name;
        start_time = jsonObj.start_time;
        end_time = jsonObj.end_time;
        type = jsonObj.type;
        cancelled = jsonObj.cancelled;
        shift_id = jsonObj.shift_id;

        table_data += '<tr><td>'+no+'</td><td>'+doctor_name+'</td><td>'+start_time+'</td><td>'+end_time+'</td><td>'+type+'</td>';
        if(type == 'Regular') 
        {
          if(cancelled == 1)
          {
            table_data += '<td>Cancelled</td>';
            
          }
          else
          {
            table_data += '<td>Active</td>';
          }
        }
        else if(type == 'Special')
        {
          if(cancelled == 1)
          {
            table_data += '<td>Cancelled</td>';
          }
          else
          {
            table_data += '<td>Active</td>';
          }
        }
        table_data += '</tr>';
    }
    table_data += '</table>';
    document.getElementById('table_div').innerHTML = table_data;
}


function setToInit()
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
    $('#txt_date').text('--');
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}


</script>
<title>Add temporary shift</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php
   $msg = $this->session->flashdata('msg');
   if(isset($msg)) echo $msg; 
   ?></h3>
</div>
<div>
<h2>Temporary shift information</h2>
</div>
<div>
<?php 
$data = array(
              'id' => 'from_add_temporary_shift');
echo form_open('internal/add_temporary_shift_confirm', $data); ?>
<label for="ddl_hospitals">Hospital:</label>
<?php echo form_dropdown('ddl_hospitals', $hospital_list,'', 'id="ddl_hospitals"');?>
</br>
</br>
<label for="ddl_doctors">Doctor:</label>
<?php echo form_dropdown('ddl_doctors', $doctor_list,'', 'id="ddl_doctors" disabled="disabled"');?>
</br>
</br>
<label for="date">Date:</label>
<?php 
$data = array(
              'name'        => 'txt_date',
              'id'          => 'txt_date',
              'value'       => '--',
              'size'        => '10',
              'disabled'    => true,
              'required' => 'required');
echo form_input($data);
?>
</br>
</br>
<div id="table_div"></div>
</br>
<label for="ddl_start_time">Start time:</label>
<?php
$options = array(
'6:00 am' => '6:00 am',
'6:30 am' => '6:30 am',
'7:00 am' => '7:00 am',
'7:30 am' => '7:30 am',
'8:00 am' => '8:00 am',
'8:30 am' => '8:30 am',
'9:00 am' => '9:00 am',
'9:30 am' => '9:30 am',
'10:00 am' => '10:00 am',
'10:30 am' => '10:30 am',
'11:00 am' => '11:00 am',
'11:30 am' => '11:30 am',
'12:00 pm' => '12:00 pm',
'12:30 pm' => '12:30 pm',
'1:00 pm' => '1:00 pm',
'1:30 pm' => '1:30 pm',
'2:00 pm' => '2:00 pm',
'2:30 pm' => '2:30 pm',
'3:00 pm' => '3:00 pm',
'3:30 pm' => '3:30 pm',
'4:00 pm' => '4:00 pm',
'4:30 pm' => '4:30 pm',
'5:00 pm' => '5:00 pm',
'5:30 pm' => '5:30 pm',
'6:00 pm' => '6:00 pm',
'6:30 pm' => '6:30 pm',
'7:00 pm' => '7:00 pm',
'7:30 pm' => '7:30 pm',
'8:00 pm' => '8:00 pm',
'8:30 pm' => '8:30 pm',
'9:00 pm' => '9:00 pm',
'9:30 pm' => '9:30 pm',
'10:00 pm' => '10:00 pm',
'10:30 pm' => '10:30 pm',
'11:00 pm' => '11:00 pm'
);
echo form_dropdown('ddl_start_time', $options, '4:00 pm', 'id="ddl_start_time"');
?>
</br>
</br>
<label for="ddl_end_time">End time:</label>
<?php
echo form_dropdown('ddl_end_time', $options, '4:00 pm', 'id="ddl_end_time"');
?>


</br>
</br>
<div id="table_div"></div>
</br>
<?php
  $data = array(
  'id' => 'btnRegister',
  'type' => 'submit',
  'value'=> 'Register Shifts',
  'class'=> 'submit',
  'disabled'    => true
  );
  echo form_submit($data);   
?>
<?php echo form_close(); ?>
</div>
</body>
</html>

