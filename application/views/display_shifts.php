
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#ddl_hospitals").change(function () 
  	{
		load_table();
  	});
});

function load_table() 
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';

    if($("#ddl_hospitals").val() == 0 )
    {
      return;
    }
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_get_regular_shifts_for_hospital');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }

    var dayMap = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    table_data = '<table>';
    table_data += '<tr><th>No</th><th>Doctor</th><th>Day</th><th>Start</th><th>End</th><th>LessQ ID</th><th>Confirmed</th><th>Doctor ID</th><th>Shift ID</th></tr>';

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        no = i + 1;
        doctor_name = jsonObj.display_name;
        start_time = jsonObj.start_time;
        end_time = jsonObj.end_time;
        day = dayMap[jsonObj.day];
        cancelled = jsonObj.cancelled;
        shift_id = jsonObj.shift_id;
        sms_code = jsonObj.sms_code;
        doctor_id = jsonObj.doctor_id;
        shift_id = jsonObj.shift_id;

        table_data += '<tr><td>'+no+'</td><td>'+doctor_name+'</td><td>'+day+'</td><td>'+start_time+'</td><td>'+end_time+'</td><td>'+sms_code+'</td><td><input type="checkbox"></td><td>'+doctor_id+'</td><td>'+shift_id+'</td>';
        table_data += '</td></tr>';
    }
    table_data += '</table>';
    document.getElementById('table_div').innerHTML = table_data;
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}

</script>
<title>Admin function: reset dailyq</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>
<div>
<h3> Select a hospital</h3>
</div>
<div>
<?php

	//echo form_open('internal/display_shifts_result');

    echo form_dropdown('ddl_hospitals', $hospital_list,'', 'id="ddl_hospitals"');

	// Show Email Field in View Page
	//echo form_close();
?>
</div>
<div id="table_div"></div>
</body>
</html>

