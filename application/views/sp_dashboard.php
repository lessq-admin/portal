
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    updateDashboard();
    $('.box').click(function() {  
    	var queid = $(this).attr('id');
      	window.location.href='sp_dashboard/get_que_details/' + queid;
    });
});

function updateDashboard() 
{
	var jsonData = $.ajax({
    url: "<?php echo site_url('sp_dashboard/json_get_current_status');?>",
    type: "POST",
    data: {} ,
    dataType:"json",
    async: false
  }).complete(function(){
       setTimeout(function(){updateDashboard();}, 10000);
      }).responseText;
   //alert(jsonData);
  // return;
  var data = JSON.parse(jsonData);
  //process on error messages
  if(data.error != null) {
  	$("#msgdiv").empty();
  	$("#msgdiv").append("Error has occured. </br> Please contact LessQ admin.");
    return;
  }
// alert(data.syncked);
  $("#last_syncked").text(data.syncked);

  var ques = data.display_data;
  $("#container").empty();
  for(var i = 0; i < ques.length; i++) 
  {
	var que = ques[i];
	var id = que.id;
	var sms_code = que.sms_code;
	var opr_time = que.opr_time;
	var disp_status = que.disp_status;
	var last_updated_time = que.last_updated_time;
	var status_bg = que.status_bg;

	var box_html = "<div class='box'";
	box_html += "id='"+id+"'";
	box_html += " style='background:" + status_bg + "'>";
	box_html += sms_code;
	box_html += "<div class='inner_box'>";
	box_html += "(" + opr_time + ")";
	box_html += "</br>" + disp_status;
	box_html += "</br>Updated: " + last_updated_time;
	box_html += "</div>"
	box_html += "</div>";
    $("#container").append(box_html);
  }

  //   var syncDate = jsonData.sync_date;
  //   var syncTime = jsonData.sync_time;
  //   // alert($("#span_date").text());
  //   // $("#span_date").text(syncDate);
  //   $("#span_time").text(syncTime);
  //   table = jsonData.table;
  //   document.getElementById('center_div').innerHTML = '';

  //   table_data = '<table align="center">';
  //   for(var i = 0; i < table.length; i++) 
  //   {
  //       jsonObj = table[i];
  //       no = i + 1;
  //       doctor_name = jsonObj.display_name;
  //       msg = jsonObj.msg;

  //       table_data += '<tr><td>'+doctor_name+'</td><td>'+msg+'</td></tr>';
        
  //   }
  //   table_data += '</table>';
  //   document.getElementById('center_div').innerHTML = table_data;
}

</script>
<style type="text/css">
#container {
    border: 2px dashed #444;
    height: auto;
    /* just for demo */
    width: 100%;
}

.box{
    width: 150px;
    height: 80px;
    vertical-align: top;
    display: inline-block;
    margin:5px;
    *display: inline;
    padding:10px;
    font-weight: normal;
    font-size: 20px;
    cursor:pointer;
    
}

.inner_box {
	font-weight: normal;
    font-size: 15px;
}



</style>
<title><?php echo $title?></title>
</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php if(isset($msg)) echo $msg; ?></h3>
</div>
<h1><?php echo $title?></h1>
<h3><span id="last_syncked"> 2014/22//222</span></h3>
<div id="container">
    <div class="box">
    	HHL12 
    	<div class="inner_box">
    		(2:30pm~5:30pm)
	    	Started:3:pm</br>
	    	Seeing patient:20</br>
	    	Last syncked:14:20pm
	    </div>
    </div>
    <div class="box">
    	HHL12 
    	<div class="inner_box">
    		(2:30pm~5:30pm)
	    	Started:3:pm</br>
	    	Seeing patient:20</br>
	    	Last syncked:14:20pm
	    </div>
    </div>
    <div class="box">
    	HHL12 
    	<div class="inner_box">
    		(2:30pm~5:30pm)
	    	Started:3:pm</br>
	    	Seeing patient:20</br>
	    	Last syncked:14:20pm
	    </div>
    </div>
    <div class="box">
    	HHL12 
    	<div class="inner_box">
    		(2:30pm~5:30pm)
	    	Started:3:pm</br>
	    	Seeing patient:20</br>
	    	Last syncked:14:20pm
	    </div>
    </div>
</div>
</body>
</html>