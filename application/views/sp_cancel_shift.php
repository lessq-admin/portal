
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function() {
	$("#txt_date").datepicker({ dateFormat: 'yy-mm-dd', minDate: '0' });
  $('#txt_date').bind('change', load_table);
  $("#ddl_hospitals").change(function () 
  {
      setToInit();
      loadDoctors();
      load_table();
      if($('#ddl_hospitals').val() == 0)
      {
          $( "#txt_date" ).prop( "disabled", true );
          resetDoctors();
      }
      else
      {
          $( "#txt_date" ).prop( "disabled", false );
      }
  });

  $("#ddl_doctors").change(function () 
  {
    load_table();
  });

  $('#from_cancel_shifts').on('change', ':checkbox', function () {
    // alert($('#from_cancel_shifts input:checked').length);
    if($('#from_cancel_shifts input:checked').length > 0) 
    {
      $("#btnCancel").attr("disabled", false);
    }
    else
    {
      $("#btnCancel").attr("disabled", true);
    }
  });
});

function resetDoctors()
{
    $("#ddl_doctors").prop("disabled", true);
}


function loadDoctors() 
{
    setMsgDiv("");
    $("#ddl_doctors").prop("disabled", false);
    if($('#ddl_hospitals').val() == 0)
      return;
    document.getElementById('table_div').innerHTML = '';
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_get_doctors_for_hospital');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }
        
    var ddl_doctors = $("#ddl_doctors");
    ddl_doctors.empty();
    $.each(jsonArry, function() {
        ddl_doctors.append($("<option />").val(this.doctor_id).text(this.display_name));
        //alert(this.display_name);
    });
}

function load_table() 
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';

    if($("#ddl_hospitals").val() == 0 || !Date.parse($("#txt_date").val()))
    {
      return;
    }
    var jsonData = $.ajax({
        url: "<?php echo site_url('sp_help_desk/json_get_shifts_for_hospital_doctor_date');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val(), 'ddl_doctors':  $("#ddl_doctors").val(), 'txt_date' : $("#txt_date").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    console.log(jsonData);
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }

    table_data = '<table>';
    table_data += '<tr><th></th><th>No</th><th>Doctor</th><th>Start</th><th>End</th><th>Type</th><th>Status</th></tr>';

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        no = i + 1;
        doctor_name = jsonObj.display_name;
        start_time = jsonObj.start_time;
        end_time = jsonObj.end_time;
        type = jsonObj.type;
        status = jsonObj.status;
        cancelled = jsonObj.cancelled;
        shift_id = jsonObj.shift_id;

        table_data += '<tr>';
        if(status == 1)
        {
          table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" value="' + type + '_' + shift_id + '">';
        }
        else
        {
          table_data += '<td>';          
        }

        table_data += '</td>';

        table_data += '<td>'+no+'</td><td>'+doctor_name+'</td><td>'+start_time+'</td><td>'+end_time+'</td><td>'+type+'</td>';

        //shift status
        //0:cancel, 1:active, 2:override
        if(status == 0) 
        {
          table_data += '<td>Cancelled</td>';
          
        }
        else if(status == 1)
        {
          table_data += '<td>Active</td>';
        }
        else if(status == 2)
        {
          table_data += '<td>Overridden</td>';
        }
        else
        {
          table_data += '<td>exception: contact chamidu</td>';
          table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" disabled ="disabled" checked="true" value="' + type + '_' + shift_id + '">';
        }


        // if(type == 'Regular') 
        // {
        //   if(cancelled == 1)
        //   {
        //     table_data += '<td>Cancelled</td>';
        //     table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" disabled ="disabled" checked="true" value="reg_'+shift_id+'">';
        //   }
        //   else
        //   {
        //     table_data += '<td>Active</td>';
        //     table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" value="reg_'+shift_id+'">';
        //   }
        // }
        // else if(type == 'Special')
        // {
        //   if(cancelled == 1)
        //   {
        //     table_data += '<td>Cancelled</td>';
        //     table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" disabled ="disabled" checked="true" value="spe_'+shift_id+'">';
        //   }
        //   else
        //   {
        //     table_data += '<td>Active</td>';
        //     table_data += '<td><input type="checkbox" id="check_list" name="check_list[]" value="spe_'+shift_id+'">';
        //   }
        // }
        table_data += '</td></tr>';
    }
    table_data += '</table>';
    document.getElementById('table_div').innerHTML = table_data;
}
function setToInit()
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
    $('#txt_date').text('--');
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}


</script>
<title>Cancel shift</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php
   $msg = $this->session->flashdata('msg');
   if(isset($msg)) echo $msg; 
   ?></h3>
</div>
<div>
<h2>Select the shift and cancel</h2>
</div>
<div>
<?php 
$data = array(
              'id' => 'from_cancel_shifts');
echo form_open('sp_help_desk/cancel_shift_confirm', $data); ?>
<label for="ddl_hospitals">Hospital:</label>
<?php echo form_dropdown('ddl_hospitals', $hospital_list,'', 'id="ddl_hospitals"');?>
</br>
</br>
<label for="date">Date:</label>
<?php 
$data = array(
              'name'        => 'txt_date',
              'id'          => 'txt_date',
              'value'       => '--',
              'size'        => '10',
              'disabled'    => true,
              'required' => 'required');
echo form_input($data);
?>
</br>
</br>
<label for="ddl_doctors">Doctor:</label>
<?php echo form_dropdown('ddl_doctors', $doctor_list,'', 'id="ddl_doctors" disabled="disabled"');?>
</br>
</br>
<div id="table_div"></div>
</br>

<?php
$data= array(
  'name' => 'txt_password',
  'class' => 'input_box'
  );
  echo 'Password: ';echo form_password($data);
  echo '<br>';
?>
</br>
<?php
  $data = array(
  'id' => 'btnCancel',
  'type' => 'submit',
  'value'=> 'Cancel Selected Shifts',
  'class'=> 'submit',
  'disabled'    => true
  );
  echo form_submit($data);   
?>
<?php echo form_close(); ?>
</div>
</body>
</html>

