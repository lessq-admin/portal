
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function() {
  $("#to_date").datepicker({ 
    dateFormat: 'yy-mm-dd', minDate: $.datepicker.parseDate("yy-mm-dd", $("#from_date").val())}
  );

  $("#from_date").datepicker({ dateFormat: 'yy-mm-dd', maxDate: '0'  }).bind("change",function(){
      var minValue = $(this).val();
      minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
      minValue.setDate(minValue.getDate());
      $("#to_date").datepicker( "option", "minDate", minValue );
  });
  $("#ddl_hospitals").change(function () 
  {
      if($('#ddl_hospitals').val() == 0)
      {
          $("#ddl_doctors").prop("disabled", true);
          $("#ddl_sms_status").prop( "disabled", false );
          $("#ddl_sms_status").val("-1");
          $('#ddl_doctors').find('option').remove().end();
          $('#ddl_doctors').append('<option value="0">--All--</option>');
          $("#ddl_doctors").val("0");
      }
      else
      {
          loadDoctors();
          $("#ddl_doctors").prop("disabled", false);
          $("#ddl_sms_status").prop( "disabled", true );
          $("#ddl_sms_status").val("1");
      }      
  });

  $("#ddl_doctors").change(function () 
  {
    load_table();
  });

  $("#check_select_all").click(function(){
    alert(1);
    
  });

  $('#from_sms_reply').on('change', ':checkbox', function () {
    if($('#from_cancel_shifts input:checked').length > 0) 
    {
      $("#btnCancel").attr("disabled", false);
    }
    else
    {
      $("#btnCancel").attr("disabled", true);
    }
  });
  // $("#btnSearch").click(function(){
  //       alert(1);
  //   $(this).find(':input').prop('disabled', false);
  //       return true;
  //   });

  $('#form_sms_reply_search_results').bind('submit', function () {
    $(this).find(':input').prop('disabled', false);
  });

});

function loadDoctors() 
{
    setMsgDiv("");
    $("#ddl_doctors").prop("disabled", false);
    if($('#ddl_hospitals').val() == 0)
      return;
    document.getElementById('table_div').innerHTML = '';
    var jsonData = $.ajax({
        url: "<?php echo site_url('internal/get_doctors_for_hospital');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val()} ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
        return;
    }
        
    var ddl_doctors = $("#ddl_doctors");
    ddl_doctors.empty();
    $.each(jsonArry, function() {
        ddl_doctors.append($("<option />").val(this.doctor_id).text(this.display_name));
    });
}

function loadTable()
{
  var jsonData = $.ajax({
        url: "<?php echo site_url('internal/json_get_sms_logs');?>",
        type: 'POST',
        data: { 'ddl_hospitals': $("#ddl_hospitals").val(), 
                'ddl_doctors'  :  $("#ddl_doctors").val(), 
                'from_date' : $("#from_date").val(),
                'to_date' : $("#to_date").val(),
                'sms_status' : $("#ddl_sms_status").val()
              } ,
        dataType:"json",
        async: false
        }).responseText;
    var jsonArry = JSON.parse(jsonData);
    // alert(jsonData);

    //process on error messages
    if(jsonArry.error != null) {
        setMsgDiv(jsonArry.error);
    }
    else
    {
      table_data = '<table>';
    table_data += '<tr><th><input type="checkbox" id="check_select_all"</th><th>No</th><th>Carrier</th><th>Data&ampTime</th><th>Tel</th><th>Request</th><th>Response</th></tr>';

    for(var i = 0; i < jsonArry.length; i++) 
    {
        jsonObj = jsonArry[i];
        no = i + 1;
        log_id = jsonObj.log_id;
        carrier = jsonObj.carrier;
        tel = jsonObj.tel;
        date_time = jsonObj.date_time;
        req_msg = jsonObj.req_msg;
        res_msg = jsonObj.res_msg;

        table_data += '<tr><td><input type="checkbox" id="check_list" name="check_list[]" value="reg_'+log_id+'"></td><td>'+no+'</td><td>'+carrier+'</td><td>'+date_time+'</td><td>'+tel+'</td><td>'+req_msg+'</td><td>'+res_msg+'</td></tr>';
    }
    table_data += '</table>';
    document.getElementById('table_div').innerHTML = table_data;
    }
}

function setToInit()
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
    $('#txt_date').text('--');
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}


</script>
<title>SMS reply</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php
   $msg = $this->session->flashdata('msg');
   if(isset($msg)) echo $msg; 
   ?></h3>
</div>
<div>
<h2>Select search criteria</h2>
</div>
<div>
<?php 
$data = array(
              'id' => 'form_sms_reply_search_results');
echo form_open('sp_core/internal_sms_reply_search_results', $data); ?>
<label for="ddl_hospitals">Hospital:</label>
<?php echo form_dropdown('ddl_hospitals', $hospital_list,'', 'id="ddl_hospitals"');?>
</br>
</br>
<label for="ddl_doctors">Doctor:</label>
<?php echo form_dropdown('ddl_doctors', $doctor_list,'', 'id="ddl_doctors" disabled="disabled"');?>

</br>
</br>
<label for="from_date">Date from:</label>
<?php 
$data = array(
              'name'        => 'from_date',
              'id'          => 'from_date',
              'value'       => $from_date,
              'size'        => '10',
              'required' => 'required');
echo form_input($data);
?>
<label for="to_date">To:</label>
<?php 
$data = array(
              'name'        => 'to_date',
              'id'          => 'to_date',
              'value'       => $to_date,
              'size'        => '10',
              'required' => 'required');
echo form_input($data);
?>
</br>
</br>
<label for="ddl_sms_status">SMS Status:</label>
<?php echo form_dropdown('ddl_sms_status', $sms_status,'', 'id="ddl_sms_status"');?>
</br>
</br>
<label for="ddl_group">Group by user:</label>
<?php echo form_dropdown('ddl_group_req', $group_req,'', 'id="ddl_group_req"');?>

<div id="table_div"></div>
</br>
<?php
  $data = array(
  'id' => 'btnSearch',
  'type' => 'submit',
  'value'=> 'Search',
  'class'=> 'submit'
  );
  echo form_submit($data);   
?>
<?php echo form_close(); ?>
</div>
</body>
</html>

