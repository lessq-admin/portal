
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function() {
  $("#check_select_all").change(function(){
    if ($("#check_select_all").is(':checked')) {
            $("#table_div input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#table_div input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
  });

  $('#txt_sms').keyup(function () {
    var max = 160;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text(' you have reached the limit');
    } else {
      var char = max - len;
      $('#charNum').text(char + ' characters left');
    }
    if(len > 0 & $('#table_div input:checked').length > 0) {
      $("#btnSend").attr("disabled", false);
    }
    else {
      $("#btnSend").attr("disabled", true);
    }

  });

  $('#table_div').on('change', ':checkbox', function () {
    if($('#table_div input:checked').length > 0 & $('#txt_sms').val().length > 0) 
    {
      $("#btnSend").attr("disabled", false);
    }
    else
    {
      $("#btnSend").attr("disabled", true);
    }
  });
  $("#btnSearch").click(function(){
        loadTable();
        return false;
    });

});




function setToInit()
{
    setMsgDiv("");
    document.getElementById('table_div').innerHTML = '';
    $('#txt_date').text('--');
}

function setMsgDiv(msg) 
{
    document.getElementById('msgdiv').innerHTML = '<h3>' + msg + '</h3>';
}


</script>
<title>SMS reply search results</title>

</head>
<body>
<div style="color:#FF0000" id="msgdiv">
	<h3><?php
   $msg = $this->session->flashdata('msg');
   if(isset($msg)) echo $msg; 
   ?></h3>
</div>
<div>
<h2>Search criteria</h2>
</div>
<div>
<?php
$data = array(
              'id' => 'form_sms_reply_confirm'); 
echo form_open('sp_core/internal_sms_reply_confirm'); ?>
<label>Hospital:</label>
<label><?php echo $selected_hospital ?></label>
</br>
</br>
<label>Doctor:</label>
<label><?php echo $selected_doctor ?></label>
</br>
</br>
<label>Date from:</label>
<label><?php echo $selected_from_date ?></label>
<label>To:</label>
<label><?php echo $selected_to_date ?></label>
</br>
</br>
<label>SMS Status:</label>
<label><?php echo $selected_sms_status ?></label>
</br>
</br>
<div id="table_div">
<?php
  if(count($tbl_data) > 0)
  {
    print '<table style="margin:5px;">';
    print '<tr><th><input type="checkbox" id="check_select_all"/></th><th>No</th><th>Carrier</th><th>Data&ampTime</th><th>Tel</th><th>Request</th><th>Response</th></tr>';
    $cnt = 1;
    // print_r($tbl_data);
    foreach($tbl_data as $row) 
    {
      print '<tr><td><input type="checkbox" id="check_list" name="check_list[]" value="'.$row->log_id.'"</td><td>'.$row->no.'</td><td>'.$row->carrier.'</td><td>'.$row->date_time.'</td><td>'.$row->tel.'</td><td>'.$row->req_msg.'</td><td>'.$row->res_msg.'</td></tr>';
    }
    print '</table>';
  }
  else {
    print '<h2>Empty Result</h2>';
  }

?>
</div>
</br>
<div> SMS message <span id="charNum">160 characters left</span></div>
<?php

$data = array(
      'name'        => 'txt_sms',
      'id'          => 'txt_sms',
      'rows'        => '8',
      'cols'        => '10',
      'maxlength'   => '160',
      'style'       => 'width: 500px;',
    );

  echo form_textarea($data);
?>
</br>
</br>
<?php
  $data = array(
  'id' => 'btnSend',
  'type' => 'submit',
  'value'=> 'Send',
  'class'=> 'submit',
  'disabled'    => true
  );
  echo form_submit($data);   
?>
<?php echo form_close(); ?>
</div>
</body>
</html>

