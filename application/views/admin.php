
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title?></title>
</head>
<body>
<h1><?php echo $title?></h1>
<div>
	<ul>
	  <li><a href="<?php echo site_url('internal/reset_dailyq'); ?>">Reset daily queue</a></li>
	  <li><a href="<?php echo site_url('internal/display_shifts'); ?>">Display shifts</a></li>
	  <li><a href="<?php echo site_url('internal/show_sms_log'); ?>">Daily SMS usage</a></li>
	  <li><a href="<?php echo site_url('internal/sms_filter'); ?>">SMS Filter</a></li>
	  <li><a href="<?php echo site_url('internal/q_operation_logs'); ?>">Q Operation Logs</a></li>
	  <li><a href="<?php echo site_url('internal/add_temporary_shift'); ?>">Add temporary shift</a></li>
	  <li><a href="<?php echo site_url('internal/cancel_shift'); ?>">Cancel shift</a></li>
	  <li><a href="<?php echo site_url('internal/gen_device_uid'); ?>">Generate device UID</a></li>
	  <li><a href="<?php echo site_url('internal/show_devices'); ?>">Display Devices</a></li>
	  <li><a href="<?php echo site_url('internal/show_bookings'); ?>">Show Bookings</a></li>
	  <!--li><a href="<?php echo site_url('internal/sms_reply'); ?>">SMS Reply</a></li-->
	  <!--li><a href="#">Coming</a></li>
	  <li><a href="#">Coming</a></li-->
	</ul>

</div>
</body>
</html>

