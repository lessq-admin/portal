
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title?></title>
</head>
<body>
<h1><?php echo $title?></h1>
<div>
	<ul>
	  <li><a href="<?php echo site_url('sp_help_desk'); ?>">Help Desk</a></li>
	  <li><a href="<?php echo site_url('sp_reports'); ?>">Reports</a></li>
	  <li><a href="<?php echo site_url('sp_back_office'); ?>">Back Office</a></li>
	  <li><a href="<?php echo site_url('sp_core'); ?>">Core</a></li>
	  <li><a href="<?php echo site_url('sp_operation'); ?>">Operations</a></li>
	  <li><a href="<?php echo site_url('sp_dashboard'); ?>">Dashboard</a></li>
	  <li><a href="<?php echo site_url('sp_pdu'); ?>">PDUs</a></li>
	  <li><a href="<?php echo site_url('pduservice/pdu_process'); ?>">PDU single</a></li>
	  <!--li><a href="<?php echo site_url('internal/sms_reply'); ?>">SMS Reply</a></li-->
	</ul>

</div>
</body>
</html>