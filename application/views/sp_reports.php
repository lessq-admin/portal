
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title?></title>
</head>
<body>
<h1><?php echo $title?></h1>
<div>
	<ul>
		<li><a href="<?php echo site_url('sp_reports/hospital_timetable'); ?>">Hospital Time Table</a></li>
		<li><a href="<?php echo site_url('sp_reports/daily_sms_count'); ?>">Daily SMS Count</a></li>
		<li><a href="<?php echo site_url('sp_reports/subscriber_sms_count'); ?>">Subscriber SMS Count</a></li>
		<li><a href="<?php echo site_url('sp_reports/q_operation_chart'); ?>">Q Operation Chart</a></li>
	</ul>

</div>
</body>
</html>